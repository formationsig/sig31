# Summary

## SIG perfectionnement 3.1: BDD Relationnelle & Spatiale

* [Préambule](/pas_a_pas/SIG31_BDD.md)

* [Contexte](/pas_a_pas/SIG31_BDD.md#contexte)
* [Pourquoi choisir Spatialite ?](/pas_a_pas/SIG31_BDD.md#pourquoi-choisir-spatialite-)
* [Le langage SQL](/pas_a_pas/SIG31_BDD.md#le-langage-sql)
* [Les spécificités SQLite / Spatialite](/pas_a_pas/SIG31_BDD.md#les-spécificités-sqlite--spatialite)
* [...et QGIS dans tout ça...](/pas_a_pas/SIG31_BDD.md#et-qgis-dans-tout-ça)

* [1. Rappels sur les bases de données relationnelles](/pas_a_pas/SIG31_BDD.md#1-rappels-sur-les-bases-de-données-relationnelles)
	* [1.1. Une table](/pas_a_pas/SIG31_BDD.md#11-une-table)
	* [1.2. Des relations](/pas_a_pas/SIG31_BDD.md#12-des-relations)
	* [1.3. MCD](/pas_a_pas/SIG31_BDD.md#13-mcd)
	* [1.4. MLD](/pas_a_pas/SIG31_BDD.md#14-mld)

* [2. Présentation du jeu de données](/pas_a_pas/SIG31_BDD.md#2-présentation-du-jeu-de-données)
	* [2.1. Le contexte](/pas_a_pas/SIG31_BDD.md#21-le-contexte)
	* [2.2. La base de données](/pas_a_pas/SIG31_BDD.md#22-la-base-de-données)

* [3. Création de la base de données](/pas_a_pas/SIG31_BDD.md#3-cr%C3%A9ation-de-la-base-de-donn%C3%A9es)
	* [3.1. Paramétrage et vérifications](/pas_a_pas/SIG31_BDD.md#31-paramétrage-et-vérifications)
	* [3.2. Création d'une base de données Spatialite](/pas_a_pas/SIG31_BDD.md#32-création-d-une-base-de-donnees-spatiale)
	* [3.3. Une base de données vierge ?](/pas_a_pas/SIG31_BDD.md#33-une-base-de-données-vierge-)
	* [3.4. Import de tables et données](/pas_a_pas/SIG31_BDD.md#34-import-de-tables-et-données)
	* [3.5. Créer une table vierge](/pas_a_pas/SIG31_BDD.md#35-créer-une-table-vierge)
	* [3.6. Création des relations dans le projet QGIS](/pas_a_pas/SIG31_BDD.md#36-création-des-relations-dans-le-projet-qgis)
		* [3.6.1. Les relations de 1 à 1](/pas_a_pas/SIG31_BDD.md#361-les-relations-de-1-à-1)
		* [3.6.2. Les relations de 1 à n](/pas_a_pas/SIG31_BDD.md#362-les-relations-de-1-à-n)
		* [3.6.3. Les relations de n à n](/pas_a_pas/SIG31_BDD.md#363-les-relations-de-n-à-n)

* [4. Création de formulaires](/pas_a_pas/SIG31_BDD.md#4-création-de-formulaires)
	* [4.1. Un formulaire simple](/pas_a_pas/SIG31_BDD.md#41-un-formulaire-simple)
	* [4.2. Un formulaire complet](/pas_a_pas/SIG31_BDD.md#42-un-formulaire-complet)
	* [4.3. Afficher les relations dans les formulaires](/pas_a_pas/SIG31_BDD.md#43-afficher-les-relations-dans-les-formulaires)
	* [4.4. Le contrôle de visibilité](/pas_a_pas/SIG31_BDD.md#44-le-contrôle-de-visibilité)
* [EXERCICE de réappropriation](/pas_a_pas/SIG31_BDD.md#exercice)
	* [4.5. Thèmes de carte](/pas_a_pas/SIG31_BDD.md#45-thèmes-de-carte)
	* [4.6. La question des images](/pas_a_pas/SIG31_BDD.md#46-la-question-des-images)

* [5. La saisie](/pas_a_pas/SIG31_BDD.md#5-la-saisie)

* [6. Interroger la base de données](/pas_a_pas/SIG31_BDD.md#6-interroger-la-base-de-données)
	* [6.1. Interrogations simples et rappels](/pas_a_pas/SIG31_BDD.md#61-interrogations-simple-et-rappels)
	* [6.2. Utilisation du Gestionnaire de Base de Données](/pas_a_pas/SIG31_BDD.md#62-utilisation-du-gestionnaire-de-base-de-données)
	* [6.3. Interroger plusieurs tables](/pas_a_pas/SIG31_BDD.md#63-interroger-plusieurs-tables)
	* [6.4 Création d'une vue SQL](/pas_a_pas/SIG31_BDD.md#64-cr%C3%A9ation-dune-vue-sql)
	* [6.5. Les styles](/pas_a_pas/SIG31_BDD.md#65-les-styles)

* [Conclusion](/pas_a_pas/SIG31_BDD.md#conclusion)

* [Ressources](/pas_a_pas/SIG31_BDD.md#ressources)
