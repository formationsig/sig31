# ![SIG31](images/sig31.png) SIG 3.1: BDD Relationelle & Spatiale



## Tables attributaires des données d'exercices



### F108633_Saran_Motte_Petree.sqlite

![img](images/qgis24.png) Panneau couche   
 

![img][006]   

![img](images/iconDB.png) Gestionnaire de BDD   


![img][001]   

*t_fait* ![img][002]      

*t_us*  ![img][003]    

*t_photo*  ![img][004]   

*t_mob*  ![img][005]   


[001]:images/tables/arbo.png	"arborescence de la BDD dans DB Manager"
[002]:images/tables/fait.png	"extrait de la table des faits"
[003]:images/tables/us.png	"extrait de la table des US"
[004]:images/tables/photo.png	"extrait de l'inventaire photo"
[005]:images/tables/mob.png	"extrait de l'inventaire de mobilier"
[006]:images/tables/arboQGIS.png	"arborescence de la BDD dans QGIS"