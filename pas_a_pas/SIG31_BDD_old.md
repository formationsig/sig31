![SIG31](images/sig31.png) **SIG 3.1: BDD Relationnelles et spatiales**



***Pré-requis***

  - Avoir obligatoirement suivi la formation **SIG 1: Découverte** & **SIG 2: Figures**.
  - Il est recommandé d'avoir suivi **Stat1 - Statistiques descriptives univariées**
  - Utiliser régulièrement et fréquemment QGIS 
  - Être capable de créer un jeu de données “propre” (attributs et géométries)

***Objectifs de la formation***

  - Mettre en œuvre les principes de structuration des données archéologiques
  - Ne plus distinguer les données descriptives d'un côté et géométriques de l'autre
  - Concevoir et utiliser une base de données relationnelle, attributaire et spatiale
  - Faire de QGIS une interface globale et ergonomique pour l'ensemble de la donnée
  - Maîtriser les bases du langage SQL.

***Objectifs secondaires***

  - Savoir quel Système de Base de Données Relationnel (SGBDR) utiliser dans un projet archéologique parmi les possibilités offertes par l'institut
  - Faire du Gestionnaire de Base de données son nouveau meilleur ami :dog:
  - Comprendre une requête SQL complexe  :robot: (mobilisant plusieurs tables et des opérateurs géométriques)

***Jeux de données***

  - **F108633\_Saran\_Motte\_Petree** : Manipulation du jeu de donnée puis reconstruction de la BDD SQLite/Spatiallite, création de tables à partir de fichiers simples ou ex nihilo.
  - **Données des stagiaires**: si le temps le permet travail en atelier sur les données des stagiaires: : définition de problématiques, préparation et structuration des données.

### Contexte

Cette formation est à l'origine un *Atelier Archéomatique* intitulé [Création et utilisation d’une base de données relationnelle, attributaire et spatiale avec les logiciels QGIS et SQLite/Spatialite](https://isa.univ-tours.fr/2018/12/16/creation-et-utilisation-dune-base-de-donnees-relationnelle-attributaire-et-spatiale-avec-les-logiciels-qgis-et-sqlite-spatialite/). Cet atelier a été construit et présenté par Thomas Guillemard (Inrap), Julien Courtois (Pôle d’Archéologie de la ville d’Orléans, Lat) & Sylvain Badey (Inrap, Lat).

Les [ateliers archéomatiques](https://isa.univ-tours.fr/category/archeomatique/) permettent de réunir durant une journée des archéologues autour de l’application d’une méthode ou d’un outil informatique afin de l’appréhender, d’en cerner l’intérêt et de l’appliquer. Une session d’ateliers peut éventuellement se dérouler sur plusieurs jours avec un sujet par jour.

github archeomatic

Ils sont organisés dans le cadre du réseau ISA (Information Spatiale et Archéologie) à la MSH de Tours et coordonnés par Amélie Laurent-Dehecq (CD45/CITERES-LAT) et Sylvain Badey (Inrap/CITERES-LAT).
![](images/AA2019.png)

Le fort intérêt que le sujet a suscité auprès d'archéologues de différentes institutions et de toute la France a amené à proposer deux sessions en mars puis en décembre 2019. De nombreux collègues de l'Inrap ayant en outre demandé à y participer, le réseau des référents et formateurs SIG a décidé d'en faire une formation de perfectionnement SIG.
Le sujet de la structuration et des bases de données relationnelles et spatiales faisait auparavant parti d'une formation incluant ces thèmes et l'analyse spatiales par le traitement des données SIG. 
Les thématiques de "perfectionnement" en SIG ont été scindées en deux formations distinctes mais somme toute complémentaires : d'un part **SIG 3.1** *Bases de données relationnelles et spatiales* et d'autre part **SIG 3.2** *Analyses spatiales*.
![](images/diag_formation.png)

### Programme de la formation

**Jour :one:**

  - Présentation du SGBDr SQLite/Spatialite (avantages, inconvénients, comparaison avec PostgreSQL/PostGIS). Interaction avec le logiciel QGIS.
  - Structuration et base de données.
  - Présentation du jeu de données et étude de cas.
  - Manipulation du jeu de données fourni : nettoyage, préparation, structuration/relations, interface/formulaires par glisser-déposer, interrogation/requête.

**Jour :two:**

  - Développement d’une interface de saisie et d’interrogation à l'aide des formulaires.
  - Interrogations et requêtes de la base de Données SQLite à l'aide du gestionnaire de BD ![](images/iconDB.png).
  - **option** : Atelier collaboratif à partir des données apportées par les participants (ou un jeu de données fourni) : définition de problématiques, préparation et structuration des données.

### Pourquoi choisir Spatialite ?

Les archéologues travaillent depuis longtemps sur **des bases de données plus ou moins complexes et/ou compliquées**. Ces bases de données ont été créées sur des environnements logiciels variés, que ce soit à l'Inrap ou ailleurs. Dans la majorité des cas, ces systèmes ne sont conçus que pour administrer, gérer et interroger les **données descriptives**.
Assez récemment, et notamment à l'Inrap grâce au **déploiement des SIG** au sein de l'Institut et au programme de formations à succès qui lui est associé, les chercheurs ont découvert le potentiel des SIG dans leur pratique quotidienne, pour gérer et traiter leurs **données spatiales**. 
On a donc **deux espaces de données**, l'un attributaire et l'autre spatiale, mais il s'agit bien en réalité de la même donnée archéologique, observée sur le terrain et décrite, ou étudiée et décrite. 
Le SIG permet bien sûr, et on le voit dans les formations et dans notre quotidien, de faire communiquer ces données descriptives et spatiales. Néanmoins, **cela peut s'avérer quelque peu contraignant si la problématique est complexe, si les questions sont nombreuses ou encore si le corpus nécessite une structuration de données particulière**.
Par conséquent, il est tentant pour les archéologues de vouloir recréer un **environnement de base de données directement dans le SIG**, c'est un dire, pour résumer, un espace de travail où donnée attributaire et données spatiales ne ferait qu'un.
De plus, au fur et à mesure des chantiers, des opérations, **la masse de données s'accumule**. Dans un environnement classique de SIG, une couche se compose de **plusieurs fichiers** stockés sur un *device*. Et il ne s'agit là que de la donnée spatiale ; si on ajoute les données descriptives, sous forme de tableaux, de textes, de bases de données diverses... L'utilisation classique des SIG peut aussi limiter **les potentiels traitements auxquels on pourrait/voudrait avoir accès**.
Enfin, l'utilisation de logiciel libre permet finalement une assez grande liberté.

### Le langage SQL

Le SQL (Structured Query Language) est un langage permettant de communiquer avec une base de données. Ce langage informatique est notamment très utilisé par les développeurs web pour communiquer avec les données d’un site web. <https://sql.sh/>

![SQLcontent](images/image001.png)

Le **SQL (sigle de Structured Query Language)**, en français langage de requête structurée) est un langage informatique normalisé servant à exploiter des bases de données relationnelles. La partie langage de manipulation des données de SQL permet de rechercher, d'ajouter, de modifier ou de supprimer des données dans les bases de données relationnelles.
<https://fr.wikipedia.org/wiki/Structured_Query_Language>

Chaque SGBD possède ses propres spécificités et caractéristiques. Pour présenter ces différences, des logiciels de gestion de bases de données sont cités, tels que : MySQL ![logoMySql](images/logoMySql.png), PostgreSQL ![logo PG](images/logoPG.png), SQLite ![logo SQlite](images/logoSQLite.png) , Microsoft SQL Server ![logo MSSQL](images/logoMicrosoft.png) ou encore Oracle.

La plupart des logiciels de base de données *grand public* fonctionnent sur le langage SQL ou apparenté (MS Access ![logo MS Access](images/logoMSAccess.png), LibreOffice Base ![logo LOBase](images/logoBase.png)...). A l'exception de FileMaker Pro ![logoFMP](images/logoFMP.png) qui se prétend comme tel, mais qui n'est pas un "véritable SGBDR" car il n'exploite pas le SQL pour son fonctionnement. 
La gestion de la base de données s’effectue via ce langage informatique normalisé, le langage SQL, mais une grande partie de cette gestion peut être effectuée par des logiciels disposant d’une interface graphique assimilant le code SQL.

### Les spécificités SQLite / Spatialite

![logo SQlite](images/logoSQLite.png) **SQLite** est un moteur de base de données. Il est conçu pour implémenter une grande partie du langage SQL. Il n'y a rien à installer puisque ce système est directement intégré au logiciel qui l'exploite. Le système et son code source font parties du domaine public ce qui implique une utilisation libre et évolutive.
Grâce à son environnement intégré au programme et le fait que le code source ne soit régit par aucune licence, SQLite est utilisé dans de nombreux logiciels et systèmes bien connus tels que Firefox, Skype, Android, l’iPhone et divers produits et projets ([sql.sh](https://sql.sh/sgbd/sqlite)). Parmi ce "divers produits", on trouve bien évidemment QGIS.
Il est donc **multiplateforme et libre**.

> <u>Note</u>: Par exemple, on peut l'utiliser avec des appareils avec l'OS Android grâce application DB Browser ou [SQlite Manager](https://play.google.com/store/apps/details?id=com.xuecs.sqlitemanager&hl=fr)

SQLite est "léger", par conséquent il "manque" certaines fonctions par rapport à d'autres SGBDR comme PostgreSQL et MySQL. Néanmoins pour notre utilisation, cette perte est compensée par un interfaçage avec QGIS. 

Contrairement à PostgreSQL qui est un système client/serveur qui nécessite d'être administré (par exemple pour la gestion des droits d'accès), SQLite se présente sous la forme d'**1 seul fichier** au format **.sqlite** , il est donc léger et transportable. Tout est compris dans le fichier unique : la structure de la base avec les définitions des tables, des champs, des clés... ainsi que la donnée elle-même. 

:warning: Ce fichier unique est à la fois un avantage et un inconvénient, car son caractère multi-utilisateurs s'en trouve fortement limité.

Dans QGIS, on parlera beaucoup plus souvent de **Spatialite** que de **SQLite**.
![logo Spatialite](images/logoSpatialite.png) **Spatialite** est la composante spatiale de ![sqlite](images/logoSQLite.png) **SQLite** (l'équivalent de **PostGis** pour ![PG](images/logoPG.png) **PostgreSQL**).
→ Il permet d'ajouter à SQLite la gestion des coordonnées, la géométrie, systèmes de projection, etc...

### ... et QGIS ![](images/qgis24.png) dans tout ça...

Tout l'intérêt est là :heart_eyes::heart_eyes::heart_eyes: !!!
QGis peut servir d'interface une base de données Spatialite ![](images/spatialite.png) ou PostGis ![](images/postgis.png). 
Concernant Spatialite, le logiciel permet la **création et l'implémentation** complète de la base de données sans avoir nécessairement besoin de passer un logiciel tiers. QGis offre également la possibilité de recréer un **environnement ergonomique** pour le base (gestion des formulaires, des sous-tables...) et permet, via le langage SQL, d'interroger les **données attributaires et/ou spatiales**.


# 1\. Rappels sur les bases de données relationnelles

Une base de donnée regroupe un ensemble d'information organisé d'une certaine manière afin d'être facilement accessible, consultable et interrogeable. Elle est structurée en une ou plusieurs tables de données, le plus souvent plusieurs. Les données des différentes tables sont liées en elles par la mise en place de relations. 

## 1.1. Une table

Une table est constituée d'un ou plusieurs champs. On pourra également utiliser les termes de colonne ou encore variable. Ces champs sont représentés en colonne.
Chaque champ, lors de sa création, est défini par plusieurs choses : 

- le nom : il doit être court et le plus explicite possible. Pas d'accent, pas de caractère spécial, pas d'espace.
- le type : numérique (entier ou réel), texte...
- la longueur : nombre maximum de caractère alphanumérique autorisé.
- Il peut y avoir des contraintes associées au champ : on peut décider d'interdire les doublons, chaque valeur sera alors unique ; il est possible de rendre la saisie du champ obligatoire ; une valeur par défaut (avec ou sans conditions) ; ...

La définition et la structuration de ces champs est importantes, elle doit être réfléchie en amont avant la saisie de la donnée. On s'attachera tout particulièrement à n'avoir qu'une seule information par champ. 

Au lieu de :

| Fait | Identification |
| :--: | -------------- |
| 101  | TP circulaire  |
| 102  | Fosse ovale    |

On aura : 

| Fait | Identification | Forme      |
| :--: | -------------- | ---------- |
| 101  | TP             | circulaire |
| 102  | Fosse          | ovale      |

Ou encore, au lieu de : 

| Fait | Identification |
| :--: | -------------- |
| 101  | TP  |
| 102  | Fosse  |
| 103  | TP avec calage    |

On préfèrera :

| Fait | Identification | Calage      |
| :--: | -------------- | ---------- |
| 101  | TP             | 0 |
| 102  | Fosse          |       |
| 103  | TP          | 1      |

Chaque ligne du tableau, ou de la table, correspond à un enregistrement, une entité. 
Chacun de ces enregistrement doit être caractérisé par un identifiant unique, ou, plus précisément,  ce que l'on appelle une **clé primaire** ![pk](images/icon_pk.png). Chaque enregistrement de cette clé primaire![pk](images/icon_pk.png) doit être unique et ne doit pas contenir de valeur nulle (``NULL``). Cette clé est la plupart du temps numérique et peut être le cas échéant auto incrémentée.
En théorie, en archéologie, on enregistre (presque :innocent: ) TOUT par un numéro, que ce soient des entités archéologiques (unité stratigraphique, fait ou mobilier...) ou des entités techniques (tranchée, sondage, carré de fouille...). Dans la pratique, on pourrait considérer que ces numéros sont en fait les clés primaires![pk](images/icon_pk.png) des tables de données correspondantes, mais dans le fait, ce n'est parfois pas aussi simple (les raisons et explications peuvent être multiples).
Par conséquent, on peut prendre l'habitude de laisser la base de données gérer ses propres clés primaires![pk](images/icon_pk.png). Pour chaque table, on aura alors une véritable clé primaire![pk](images/icon_pk.png), strictement structurelle, alors que l'archéologue aura à sa charge (selon ses principes et méthodes) la gestion des numéros d'enregistrement des entités archéologiques et techniques.

![11\_table](images/11_table.png)


## 1.2. Des relations

Les relations d’une base données sont reliées par certaines des valeurs qu’elles contiennent : chaque enregistrement d’une table de données contient un groupe d’informations relatives à un sujet et les différents sujets des tables sont connexes. 
> Par exemple, les tables FAITS et US sont connectées puisque l’objectif de la base de données est de gérer les US des produits !

Les différents liens de cardinalité : 
- **relation de 1 à 1** : signifie que pour chaque enregistrement d’une table il ne peut y avoir que 0 ou 1 enregistrement d’une autre table qui lui soit lié (ex : entre la table des faits et une table mobilier…). Dans une base de données classique, ce type de relation peut être évitée en la remplaçant par une la fusion (jointure ?) des deux tables en relation.
- **relation de 1 à *n*** (de un à plusieurs) : signifie que pour chaque enregistrement d’une table, il peut y avoir un ou plusieurs enregistrements d’une autre table qui lui soit lié (ex : entre la table des faits et la table des US).
- **relation de *n* à *n*** : (de plusieurs à plusieurs) : une relation existe quand un ou plusieurs enregistrements d’une table peuvent avoir une relation avec un ou plusieurs enregistrements d’une autre table (ex : entre la table des faits et la table des photos, où un fait peut être visible sur plusieurs clichés et où l’on peut voir plusieurs faits sur une photo). Dans le cas où l’on souhaite établir une relation de plusieurs à plusieurs entre deux tables, une troisième table est nécessaire pour stocker les combinaisons créées par la relation. Ce type de relation (*n* à *n*) revient à établir deux relations de un à plusieurs (1 à *n*).

Dans le cas d'une relation de 1 à *n* entre deux tables (et par extension d'une relation de *n* à *n*, où le principe reste le même), une des tables doit posséder une clé étrangère![pk](images/icon_fk.png), c'est à dire un attribut dont les valeurs sont des références à la clé primaire![pk](images/icon_pk.png) de l'autre table.


## 1.3. MCD

> MCD = Modèle Conceptuel de Données

:memo:***Quelques rappels historiographiques et conceptuels*** :
- L’enregistrement archéologique est une démarche scientifique dont l’objectif est de conserver la mémoire des vestiges fouillés (mobilier, immobilier, sédimentaires…).
- C'est une simplification du monde réel qui permet une restitution argumentée et interprétée de la stratification anthropique et naturelle fouillée, donc détruite.
- Les principes de l’enregistrement archéologique ont été édictées en Angleterre dans les années 60-70 et sont fondés sur le principe de superposition des strates géologiques mis en place au 17e siècle (et non remis en cause depuis).
- Les principes de l’enregistrement stratigraphique sont diffusés en France dès le début des années 70 et appliqués sur quelques sites emblématiques (Tours, Paris, Lattes...); matrice de Harris développée ensuite.
- La diffusion du « modèle harissien » est incomplète dès le départ, en France.

:memo:***Plusieurs approches***

**Les différentes approches méthodologiques (stratigraphiques et spatiales)** pratiquées par les archéologues en fonction des périodes, des problématiques, des habitudes, ne sont pas contradictoires, mais elles s’insèrent dans un schéma logique de données. 
>FAIT ↔ US ↔ Artefact.

De manière résumée et peut-être un peu réductrice, trois grandes approches cohabitent depuis une bonne trentaine d’années (d’après [Desachy 2008](https://tel.archives-ouvertes.fr/tel-00406241v2)) :
- l’analyse spatiale large : fouille en grand décapage dans la lignée des travaux des protohistoriens ;
- l’analyse spatiale fine type « ethnographique » (en référence à Leroi-Gourhan) ;
- un niveau médian : l’analyse par US pour les stratifications d’origine naturelle et/ou anthropique denses.

Avant les SIG, l’accès à la vision en plan passait uniquement par le plan dessiné/illustré (plan masse, minute) : le plan illustrait le discours et les descriptions. Les spécificités morphologiques et topographiques étaient prises en compte mais pas forcément de manière systématique, dynamique et structurée.

Avec la généralisation des SIG, on passe de l’illustration au traitement de données : **la forme que l’on dessine dans l’espace devient elle-même une “donnée” qui n’est plus uniquement définie par sa description mais se définit aussi par des coordonnées et une position relative par rapport aux autres structures**.

D’un point de vue très concret, l’introduction des SIG a permis de prendre en compte et d’exploiter l’espace avant même sa description. Si cette manière de faire convient bien aux pratiques déjà en vigueur dans le cadre d’une “analyse spatiale large” et dans une moindre mesure (?) à l’analyse spatiale fine dite “ethnographique” où l’espace est une entrée privilégiée, elle “convient” moins aux opérations en milieu densément stratifié où la recherche/description/compréhension de la structure précède généralement sa représentation spatiale. C’est là que se pose plus spécifiquement la question de l’articulation entre l’enregistrement stratigraphique descriptif et l’enregistrement spatial (problème des unités qui n’ont pas de représentation spatiale).

Et quand les trois cas de figure se présentent sur un même chantier, il faut "jongler". Charge à l’archéologue de s’adapter et d’adapter son enregistrement spatial et descriptif à la stratification et la densité des structures.

:memo:**Le coeur de tout système d'enregistrement archéologique: la trilogie Fait, US, artefact**

> :point_right: Restituer un MCD/MLD sommaire ensuite 
>1. le coeur de tout système
>2. le MCD simplifié d’une opération
>3. le MPD définissant les tables, les champs - dont clés primaires - et les relations).

Dans tous les systèmes, le coeur est le même : artefact, us, fait. Ensuite, on ajoute les unités techniques d’enregistrement voire des unités de regroupement archéologique.

**Etape 1 : le coeur de tout système d’enregistrement**

De manière très générale, toute notre donnée peut être répartie en trois ensembles : la donnée archéologique, le mobilier et la documentation.

![MCD](images/mcd_archeo.jpg)



Mais on ce qui concerne la donnée de terrain enregistrée, on peut résumer cela aux trois approches évoquées plus haut, qui correspondent au final à trois échelles d'analyse différentes.

![MCD](images/mcd.png)



On peut associer US et Fait, US et artefact, mais on ne peut pas avoir un enregistrement avec des Faits, des artefacts, sans US. D'un point de vue du système relationnel, ce n'est pas possible. Du point de vue de l'enregistrement purement archéologique, cela ne devrait pas être possible non plus.

> :memo:  **MCD** : un **modèle conceptuel de données** a pour but de représenter graphiquement et de la façon la plus simple et la plus directe possible l’ensemble des données qui seront analysées tout en s’affranchissant de toutes les contraintes logicielles ou liées à la base de données sur laquelle va reposer l’application. Dans le MCD, on décrit les relations avec des mots. Formalisme entité-relation.
>- Permet de modéliser la sémantique des informations d'une façon compréhensible par l'utilisateur de la future base de données.
>- Utilise le formalisme entité-relation.
>- Ne permet pas l'implémentation informatique de la base de données dans un SGBD MLD : un modèle logique de données.
>- Permet de modéliser la structure selon laquelle les données seront stockées dans le future base de données.
>- Est adaptée à une famille de SGBD (ici relationnel).
>- Utilise le formalisme Merise.
>- Permet d'implémenter la bdd dans un SGBD. **MPD** : un **modèle physique de données** est un modèle directement exploitable par la base de données utilisée. C’est le bazar réalisé. Le passage du MCD au MPD peut se matérialiser par la création de tables intermédiaires dans le cas des relations de n à n (cf. ci-dessous).

**Etape 2 : MCD sommaire**

On ajoute les unités techniques. Puis on ajoute les relations formalisées (on fait des phrases) pour établir le MCD.
![13\_mcd](images/13_mcd.png)

On peut ajouter ensuite la **dimension spatiale**, c'est à dire la géométrie des unités qui vont être représentées dans le système d'enregistrement : **points, lignes, polygones**.

![](images/mcd_geom.png)

On peut noter, dès à présent, que dans notre système d'enregistrement "TOUT EN UN " certaines entités peuvent ne pas avoir de géométrie, alors que d'autres en auront. Dans une table de faits archéologique, par exemple, où la très grande majorité des faits sont représentés spatialement dès le levé topographique, certains pourront ne jamais avoir de représentations spatiales (forme trop incertaine, fait uniquement vue en coupe...). Ainsi, dans une même table, doivent pouvoir coexister entités géométriques et entités non géométriques.
Avec Spatialite, cela est tout à fait possible. En fait, cela est possible dès l'utilisation de shapefile, mais le procédé reste contre intuitif. On reviendra sur cet aspect plus tard...


## 1.4. MLD

Le **Modèle logique de données** est une formalisation par un graphe des liens des relations entre les tables.

![14\_mld](images/14_mld.png)

> **PK** = *Primary Key* = Clé primaire ![pk](images/icon_pk.png),
> **FK** = *Foreign Key* = Clé étrangère ![fk](images/icon_fk.png)
> (l'identifiant qui permet de "remonter" à la table mère)
> 
> La table **LinkSond** est une table de jonction qui permet de stocker tous les couples relationnels entre les tables **SONDAGE**, **FAIT** & **US**.
> 
> | PK | Sond | US | Fait |
> | -- | ---- | -- | ---- |
> | 1  | 1    |    | 100  |
> | 2  | 2    |    | 100  |
> | 3  | 1    |    | 200  |
> 
> Dans cet exemple : le Fait 100 est vu dans les sondages 1 et 2


* Sur paperboard, tracer AVEC LES STAGIAIRES le graphe d'une base de données relationnelles classiques pour un enregistrement archéologique : inclure les tables Fait, Us, Mobilier, Sondage, Photo, et d'autres si besoin. 
* Prendre le temps d'expliquer le cheminement des liens de 1 à *n* et surtout de *n* à *n* avec la création de tables intermédiaires.
* Insister encore une fois sur les notions de clé primaire![pk](images/icon_pk.png) et de clé étrangère![fk](images/icon_fk.png).

# 2. Présentation du jeu de données

## 2.1. Le contexte

Pour la formation, on utilise un jeu de donnée d'une fouille. Il s'agit de la fouille de Saran "La Motte Pétrée" dans le Loiret (45).
-  Réalisée en 2016, 3 mois de terrain.
- Dirigée par Laurent Fournier (Inrap).
- Deux zones de fouilles pour **18 000 m²** ouverts.
- Périodes représentées sur le site : premier âge du Fer, période romaine et Antiquité tardive, haut Moyen Âge, Moyen Âge et période moderne.

[![rapport](images/21_rapport.png)](http://dolia.inrap.fr/flora/ark:/64298/0153263)

*Le territoire communal de Saran est, au plan archéologique, particulièrement bien documenté. Les fouilles se sont multipliées, permettant de mieux appréhender l’évolution de l’occupation de ce territoire à l’âge du Fer et aux périodes antique et médiévale qui sont représentées sur le site de « la Motte Pétrée »*.
*L’intervention réalisée au lieu-dit « la Motte Pétrée » a été prescrite par le Service régional de l’Archéologie à la suite des découvertes faites lors du diagnostic conduit par Anne-Aimée Lichon en 2011*.
*À partir des résultats de ce diagnostic, deux zones de fouilles ont été définies. Leurs emprises respectives sont centrées sur les zones de forte densité de vestiges* :
- *La zone 1 est centrée sur l’occupation du haut Moyen Âge identifiée dans la partie sud-ouest de l’emprise diagnostiquée, à proximité de la RD 577* ;
- *La zone 2 est située dans la partie nord-est de l’emprise diagnostiquée, au contact de la rue de la Motte Pétrée, où les vestiges de la période romaine étaient les plus nombreux*.

*L’intervention a démarré le lundi 29 août 2016 et s’est achevée le jeudi 10 novembre 2016. Au total 524 faits ont été identifiés au cours de cette intervention (348 faits en zone 1 et 176 en zone 2).
Quatre périodes d’occupation successives peuvent être individualisées :
Quelques vestiges attribuables au Ier âge du Fer ont été mis au jour sur les deux zones de fouille. En zone 1, l’occupation apparaît mieux structurée. Dans la partie orientale de l’emprise, les tronçons ouest et sud d’un probable enclos fossoyé ont été repérés. Au sein de cet enclos prennent place une petite construction de plan rectangulaire (L : 5,80 m ; 4,80 m ; surf. Int. : 14 m²) et quelques fosses. En zone 2, seules quelques fosses circulaires sont attribuables à cette phase d’occupation du site.
Des vestiges contemporains ont été identifiés sur les sites de la ZAC des Vergers ou sur celui de « Chimoutons » / « la Hutte » et pour le Hallstatt moyen final sur le site de la ZAC du Champ Rouge-Tranche 1. Sur les communes limitrophes, elles sont également reconnues à Ormes « rue de la Borde » (Hallstatt A) et au « Bois d’Ormes » (Hallstatt D/La Tène A). À Ingré, sur le site du « Rondeau » et à Gidy au lieu-dit « le Chêne de la Croix » d’autres vestiges de cette période ont été fouillés. Les occupations du Ier âge du Fer sont donc récurrentes autour du site de « la Motte Pétrée » même si, à l’instar de ce dernier, elles ne livrent pas des concentrations de vestiges très importantes.
Le mobilier céramique découvert dans le comblement de ces rares structures apparaît relativement homogène et place l’occupation à la fin du Hallstatt C ou au début du Hallstatt D1. La répartition des vestiges sur une aire relativement vaste, l’absence de concentration notable, à l’exception des vestiges mis au jour en zone 1, ne permet pas d’assurer de leur nature exacte ni de leur appartenance à un établissement unique.
L’environnement du site à la période romaine livre un grand nombre d’indices et de sites attestant une occupation relativement dense du secteur. À la « Motte Pétrée », les vestiges de cette période sont essentiellement présents en zone 2 où a été découvert un petit établissement agricole dont l’existence est avérée entre le Ier et le début du Ve s. apr. J.-C.
Au Ier s. apr. J.-C., les vestiges, représentés par quelques fosses, sont concentrés dans la partie occidentale de la zone 2. L’occupation structurée, ne semble intervenir qu’au cours du IIe s., période relativement tardive pour l’apparition de ce type d’établissement. Les vestiges identifiés, deux celliers, un puits et un trou de poteau, s’inscrivent au sein d’un enclos de forme rectangulaire, délimité par un fossé. La stratigraphie du cellier le plus vaste, atteste plusieurs séquences de restructuration succédant à des périodes d’abandon et de remblaiement complet ou partiel. Faut-il voir dans cette succession de phases d’occupation et d’abandon, le témoignage d’une présence humaine discontinue, peut-être en lien avec la réalisation d’une activité particulière et/ou saisonnière ? Rien cependant, dans le mobilier mis au jour ou dans les vestiges identifiés, ne permet d’en assurer.
Au cours de ce premier temps d’existence, les vestiges semblent s’étendre vers l’ouest, au-delà de l’emprise de fouille. Nous nous situerions donc, au moins pour la période du Haut-Empire romain, aux marges d’une entité plus vaste dont la nature ne peut être plus précisément définie.
Au Bas-Empire, le site ne connaît pas de transformations importantes. L’enclos fossoyé, qui conserve la forme générale et les dimensions du précédent, accueille quelques vestiges au nombre desquels on compte un vaste creusement dont la nature exacte ne peut, à ce jour, être précisée (mare, fumière ?), une construction sur poteaux, dont le plan est probablement incomplet, et trois puits ou citernes. Il convient de noter l’absence des fonds de cabane qui restent, pour cette période, l’un des « marqueurs » dans l’évolution des modes de construction. Le seul exemple de la diffusion de ce type de construction à proximité de la Loire reste, à ce jour, le site de la ZAC des Vergers.
Nous sommes donc en présence, pour la période tardo-antique, d’un établissement empruntant ses limites fossoyées à l’établissement du Haut-Empire et dont la partie centrale connaît l’installation d’un vaste creusement à la destination incertaine. Le soin apporté à la construction du radier mis au jour au fond de cette vaste structure apparaît comme un élément singulier, au même titre que les puits découverts et l’abondance du numéraire mis au jour.
Au début du haut Moyen Âge, l’occupation se déplace vers le sud-ouest (zone 1). Ce nouvel établissement est occupé sur une période comprise entre la seconde moitié du VIe et le VIIIe siècles. La majorité des vestiges sont attribuables à la période mérovingienne. L’établissement, circonscrit, au nord et à l’ouest, par des palissades, est bordé, au sud, par des fossés parallèles délimitant probablement l’emprise d’un axe de circulation. Les vestiges de l’habitat, essentiellement constitués de négatifs de poteaux, de fosses et d’un four installé à distance de l’occupation principale, sont installés sur des plates-formes mises en évidence dans la seule partie méridionale de l’emprise de fouille. Ces apports successifs de remblais viennent progressivement sceller les occupations les plus anciennes, permettant d’investir une aire plus vaste. La stratigraphie complexe qui en résulte n’a malheureusement pu être intégralement exploitée au cours de l’intervention. La période carolingienne est également représentée par quatre sépultures (deux adultes et deux enfants) mises au jour dans la partie sud-est de l’emprise.
Le Bas Moyen Âge est illustré par le creusement de vastes fosses d’extraction que le mobilier céramique abondant, mais très mêlé, permet d’attribuer aux XIIe-XIVe siècles. Enfin, un fossé découvert au pied de la limite nord de l’emprise est attribuable aux périodes médiévale et moderne.
Le site de « la Motte Pétrée » permet de confirmer la densité des occupations romaines et médiévales sur le territoire de la commune de Saran. L’identification d’un établissement de l’Antiquité tardive est particulièrement importante et vient confirmer la continuité des établissements ruraux proches d’Orléans/Cenabum, déjà vérifiée sur les sites de la Zac des Vergers et de « la Médecinerie ». Toutefois, dans le cas de « la Motte Pétrée », l’établissement mérovingien ne succède pas directement à celui de la période romaine, se déplaçant de quelques centaines de mètres vers le sud-ouest.*

*FOURNIER L. (dir.) – Centre-Val de Loire, Loiret, Saran, La Motte Pétrée. Occupations rurales successives de la fin du 1er âge du Fer à la période moderne, rapport de fouille, Pantin : Inrap CIF, 2018.- 2 vol. (977 p.) : ill. en coul., 333 figures ; 30 cm.*

![photo saran](images/20_saran.png)

![plan saran](images/22_plan.png)

## 2.2. La base de données

> La démonstration est à faire par le formateur qui va passer en revue le projet QGis et la base de données, sans aller très loin dans les détails, le but étant de montrer ce qu'on peut faire en terme de relation de table et d'ergonomie. Pour "donner l'eau à la bouche" :kiss: !!!
> Les stagiaires ont le choix : ils peuvent tester en même (mais ils n'ont pas encore toutes les clés de compréhension) ou ils peuvent suivre la démonstration :smiley:...

On ouvre d'abord le logiciel QGIS pour la démonstration du jeu de données en mode "produit fini".
On observe le contenu du jeu de données dans un explorateur Windows : un projet QGis et un fichier SQLite et c'est TOUT !!!
![fichier_saran](images/fichier_saran_bdd.png)

Toutes les couches et tables de la base de données sont dans le fichier SQLite. Rien ne change pour le projet. Il stocke les appels de couches, les différentes propriétés et déclaration de styles de celles-ci, les thèmes de cartes, les mises en pages...

![projet](images/projet_saran.png)

On découvre ensuite le potentiel en manipulant le jeu de données.

:point_right: En sélectionnant la couche *tabfait*, on utilise l'outil *Identifier des entités*![identifier](images/identifier.png). On clique sur un polygone isolé ou assez imposant (par exemple la grosse mare F.76 au centre de la zone nord-est). Si l'identification apparaît sous forme de panneau, on peut agir sur les propriétés du panneau *Résultat de l'identification*. En cliquant sur la clé à molette, on coche *Ouvrir automatiquement le formulaire si une seule entité identifiée*.

![panneau_result](images/panneau_resultat_ident.png)

En recliquant sur ![identifier](images/identifier.png) on ouvrre le formulaire de l'entité identifiée.

![form_F76](images/form_F76.png)

**Qu'observe-t-on ?**
- :one:Une fenêtre s'ouvre, elle correspond au ***formulaire*** de l'entité identifiée. Ce formulaire est défini et géré par les propriétés de la table de l'entité, *tabfait*.
- :two:Les noms des champs ne sont pas ceux d'origine, des ***alias*** leur ont été donnés afin de les rendre plus explicites dans leur utilisation.
- :three:Les champs (Rappel : ou attributs, ou variables, ou colonnes de tableau/table attributaire) sont organisés en ***onglets thématiques*** afin d'optimiser, entre autres choses, l'espace au sein du formulaire.
- :four:Des ***listes de valeurs*** ont été attribuées à certaines variables. Pratique pour respecter l'homogénéité de la saisie.
- :five:D'autres champs peut être définis en ***booléen*** (pour : vrai ou faux, présence ou absence, **1 ou 0**).
- :six:Des boutons *OK* et *Annuler* sont présents.

En parcourant les onglets, on observe des agencements de champs différents.

Sur l'onglet ***US du fait***, on constate le **résultat de la relation entre les faits et les US**, entre la table *tabfait* et la table *tabus*. Cette relation est gérée et enregistrée au niveau du projet.
On visualise donc les entités de la table fille *tabus* en relation avec l'entité de la table mère *tabfait* selon un lien de cardinalité de 1 à *n*.
Par défaut, et c'est assez intéressant et pratique, c'est le formulaire développé pour la table fille qui est embarqué.
On peut alors facilement passer du formulaire à la table attributaire filtrée en cliquant sur *Basculer sur la vue formulaire* ou sur *Basculer sur la vue tabulaire* ![form_ou_tab](images/form_ou_tab.png).

Sur l'onglet *Photo(s)*, il en est de même. Sauf qu'ici ce n'est pas une relation de 1 à *n* qui est exploitée, mais une **relation de *n* à *n* entre la table *tabfait* et la table *tabPhoto***. Cette relation complexe passe par une table intermédiaire, la table *lienPhoto*. Il s'agit bien d'une relation de plusieurs à plusieurs : un fait peut avoir été photographié plusieurs fois ET sur un même cliché, on peut observer plusieurs faits ;  on a donc une table intermédiaire qui enregistre et stocke les "couples relationnels" fait/photo.
Il est à noter que QGis passe directement de *tabfait* à *tabPhoto*.
Au niveau du "sous formulaire", en cliquant sur l'onglet "Cliché", **on peut visualiser la photo** correspondante.

![form_fait_photo](images/form_fait_photo.png)

On peut fermer le formulaire du fait F.76.

***Attardons-nous alors sur les US.***

L'identification d'une entité géométrique n'est pas le seul moyen de consulter la donnée.
On peut alors ouvrir la table attributaire ![table_attrib](images/table_attrib.png) des US.
On arrive sur la vue classique de la table attributaire de QGis. 
En appuyant sur *Basculer sur la vue formulaire* ou sur "Basculer sur la vue tabulaire" ![form_ou_tab](images/form_ou_tab.png), on peut passer d'un mode à l'autre.

![](images/form_tab_attrib.png)

Quand on entre par la table attributaire, le formulaire, une fois affiché est quelque peu différent de ce que l'on a vu précédemment.
- :one:A gauche un panneau, une sorte d'explorateur, avec la ***liste des entités***, ici les US.
- :two:En bas de ce panneau, on trouve:
  - Les ***outils classiques*** tel  que ![](images/entite_precedente_suivante.png) ( dans l'ordre: *Aller à la première entité*, *entité précédente*, *entité suivante*, *Aller à la dernière entité*). 
  - Le bouton ![](images/icon_ampoule.png) ***Mettre l'entité courante en surbrillance sur la carte***.  Ce bouton fonctionne selon le principe *Activé / désactivé*. Si l'entité est sélectionnée dans la liste et qu'elle possède une géométrie, l'entité sera mise en surbrillance sur le canevas. C'est le cas ici par exemple pour l'US 1771, mais ce n'est pas le cas pour la grande majorité des autres entités de la table des US. 
  - Les deux boutons ![](images/icon_pano_zoom.png) ***Panoramique automatique sur l'entité courant*** et ***Zoomer automatiquement sur l'entité courant***. Toujours sur le principe *Activé / désactivé*, on active ici l'un ou l'autre. Si l'entité sélectionnée possède une géométrie, l'emprise du canevas va soit se centrer sur l'entité SANS mise à l'échelle soit se centrer sur l'entité AVEC une mise à l'échelle au plus juste. Dans le cas où l'entité sélectionnée n'a pas de géométrie, une bannière jaune apparaît en haut du canevas. ![no_geom](images/no_geom.png)

- :three:Le panneau de droite montre le ***formulaire*** développé par la table courante, ici *tabus*. 
- :four:Dans le cas où un champ correspond à la clé étrangère d'une table mère (on remonte une relation de 1 à *n*), le champ a été configuré en prenant en compte cette relation. On trouve le bouton *Ouverture du formulaire*![open_mother](images/open_mother.png). Ici, cela ouvrira le formulaire du fait si l'US est lié (en relation avec) à un fait. On retrouve bien l'ergonomie que l'on souhaite avoir dans une base de données.

Chaque formulaire pour chaque couche est géré par un *style* déclaré (cf. [Les figures du rapport avec QGis](https://formationsig.gitlab.io/toc/#sig-2--les-figures-du-rapport-avec-qgis)). 

:warning: **En effet, la création des formulaires se passe au niveau des propriétés des couches. Tout comme la symbologie ou l'étiquetage, cela est enregistré suite à la déclaration d'un *style*.
On peut donc avoir plusieurs déclarations de *style* pour une couche, avec à chaque fois, des ergonomies et/ou mises en page de formulaire différentes.**
:warning: **Donc, tout comme les *styles*, on peut également profiter du potentiel de QGis en utilisant les *thèmes de carte*. Le *thème de carte* conserve les couches cochées dans l'arborescence du projet ainsi que le *style* de couche associé au moment où le *thème de carte* est enregistré.**

On observe les *thèmes de carte* en clique sur l'oeil![oeil](images/oeil.png) du panneau *Couches*.
Pour ce jeu de données, ce sont deux *thèmes de carte* qui ont été créés.

![theme_carte](images/theme_carte.png)

On utilisait précédemment le *thème de carte* **Formulaire_complet**.
Cliquer sur **Formulaire_Photo**.
En recliquant sur la mare F.76 (ne pas oublier de sélectionner la couche *tabfait*) avec l'outil Identifier les entités![identifier](images/identifier.png), on constate un formulaire beaucoup plus simple essentiellement orienté sur les photos du fait identifié.




>Pour la démonstration de la base de données de Saran, il faut présenter les formulaires avec les différents boutons, pour la plupart nouveaux pour les stagiaires. Il n'est pas nécessaire d'aller trop loin dans les explications sur le développement des formulaires, car ce sera expliqué plus tard quand on va créer de toute pièce la base de données.
>En fonction des questions, on peut parler dès à présent des relations entre les tables, des outils d'éditions des champs...
>On peut montrer dès à présent que l'on peut créer des entités sans géométries et que, le cas échéant, on peut rajouter une géométrie à l'entité ultérieurement. Mais cela sera expliquer plus en détail plus tard.
>On peut également parler de l'interrogation des données. Mais cela sera de même expliquer plus en détail plus tard, avec les différentes façons de procéder.



:clock1: **Dans l'idéal, c'est la fin de la première demi-journée** :clock1:



# 3. Création de la base de données

Dans cette partie, il s'agit de créer de toute pièce une base de données Spatialite en utilisant **![qgis](images/qgis24.png)GIS** comme outil de développement de la structure et de l'interface. 

![dossier](images/dossier_saran_brut.png)

Pour cela, le dossier *Saran brut* dans le jeu de données contient les fichiers nécessaires.
- :one:Le **dossier *Tableur*** contient des tableau d'inventaire ou des tables sans géométries : un tableau d'inventaire du mobilier archéologique, l'inventaire des photographies numériques et la table qui stocke les couples relationnels photo / sujets pour fait fonctionner la relation de plusieurs à plusieurs.
- :two:Dans le **dossier *Vecteur***, on trouvera les shapefiles de données descriptives qui possèdent des géométries : emprise, ouverture, fait, us, axe et points topo ainsi qu'une couche de parcelles cadastrales et une couche de bâti (à utiliser pour ceux qui le souhaitent, pour avoir un fond de plan et si les flux wms sont compliqués à obtenir).

Pour créer la base de données, on va tout d'abord s'appuyer sur des fichiers déjà structurés (présents dans le jeu de données), puis créer une table en partant de rien, c'est à dire en la structurant à la main.


## 3.1. Paramétrage et vérifications

Ouvrir un projet QGIS vierge

A la première utilisation de QGIS, il faut pensez à **paramétrer le système de projection de préférence au niveau du logiciel**. 
→ Préférences → Options
![option_qgis](images/option_qgis.png)

Il faut ensuite aller dans l'**onglet SCR** pour paramétrer le comportement de QGIS vis à vis des systèmes de projection.

![option_scr](images/option_scr.png)

:warning::warning: **Attention** :warning::warning: **Les couches appelées dans le projet doivent ABSOLUMENT être projetées dans le même système de projection (SCR) que le projet**. Si ce n'est pas le cas, vous risquez d'obtenir des valeurs de calcul sur les géométries fausses (superficies, longueurs, distances...). Vérifier systématiquement le système de projection des couches dans les **Propriétés/Source** de chaque nouvelle couche importée. Un référent SIG ou un topographe peut vous aidez à retrouver le SCR et éventuellement faire une reprojection. NE JAMAIS MODIFIER LE SCR EN PASSANT PAR LE MENU **Propriétés/Source** !

* Ajouter les couches :
  * F108633_emprise 
  * F108633_ouverture
  * F108633_fait
  * F108633_us
  * tabPhoto
  * tabLienPhoto

![ajout_couche](images/ajout_couche.png)

Avant de créer sa base de données à partir de fichiers existants, il est **impératif :heavy_exclamation_mark: , indispensable :warning: , obligatoire :cop: , primordial...** de :
* vérifier le **SCR** (des couche(s), du projet en cours, dans les préférences du logiciel) ;
* valider l'**encodage** (UTF-8 de préférence) ;
* vérifier l'**intégrité attributaire** et **identifier la clé primaire** ![pk](images/icon_pk.png) si elle existe... (doublons) ;
* vérifier la **validité des géométries** (nœuds et géométries en doublon, géométries invalides type polygone autosécant) ;
* pour faire simple, plus on vérifie que la données est propre, mieux c'est.

Il est possible de vérifier rapidement la présence ou l'**absence de doublon attributaire** avec l'extension **Group Stats** ![icon GS](images/iconGS.png) pour vérifier si un champ contient bien des valeurs uniques.

![](images/group_stats.png) 

**Pour vérifier la validité des géométries, on utilisera, selon les préférences et les besoins, les outils de QGIS : ![](images/verif_geom.png) ou ![](images/verif_topologie.png) ou ![](images/verif_valid.png). **
Pour plus d'informations sur ces outils, on peut se référer au pas à pas de la formation [Les figures du rapport avec QGIS](https://formationsig.gitlab.io/toc/#sig-2--les-figures-du-rapport-avec-qgis)



## 3.2. Création d'une base de données Spatialite

Il faut commencer par créer tout d'abord la base de données spatialite, c'est à dire le fichier .sqlite qui va stocker toute la donnée.
* Dans le panneau ***Explorateur***, faire un clic droit sur ***Créer une base de données***.
* Sélectionner le bon dossier et nommer le fichier SQLite correctement et faire un clic sur ***Enregistrer***.

![](images/creer_bdd.png)

> Dans notre quotidien, quel est le bon emplacement dans l'arborescence pour enregistrer la base de données ?
> Si l'on considère que la base de données reste de la donnée vectorielle, on peut très bien l'enregistrer dans le sous-dossier *vecteur_georef* du dossier *donnee_spatiale* (dans l'arborescence du dossier de l'opération courante).
> Si l'on prend conscience du changement qui s'opère dans notre quotidien, dans notre manière de travailler, d'organiser et d'interroger la donnée, on enregistrera le fichier de base de données à la racine du dossier de l'opération en cours. 
> En effet, on pourra trouver dans la même base de données : les données vectorielles issues du terrain (donnee_spatiale/vecteur_georef) mais directement associées aux descriptifs et interprétations de l'archéologue (donnee/enregistrement), les photos des contextes archéologiques et/ou du mobilier (donnee/image), les minutes de terrain (donnee/minute), les études de spécialistes (etude)... Il y a donc un véritable changement de paradigme dans notre façon de travailler, la base de données Spatialite devenant un "objet" transversal à toute l'opération.

<img src="images/Paradigm-shift-cartoon_fr.png" style="zoom:67%;" />

* Pour l'exercice, enregistrer la base de données à la racine du dossier *2_SARAN_BRUT*.
* Nommer le **SARAN.sqlite** (c'est la seule extension possible).

![](images/new_bdd.png)

A partir de maintenant, la nouvelle base de données Spatialite est disponible dans l'*Explorateur* de QGIS.
![](images/bdd_explo.png)

> Dans la majorité des cas, on aura une base de données pour une opération archéologique. 
> Pour gérer le "multisite", ou pour des bases spécialisées, en céramologie ou archéozoologie par exemple, il faudra porter une attention toute particulière à la structuration et la définition des clés primaires![pk](images/icon_pk.png) et des clés étrangères![fk](images/icon_fk.png), afin de gérer les différents contextes et éviter les doublons attributaires.


* **ENREGISTRER** le projet au même emplacement que le fichier .sqlite et **NOMMER** le comme le fichier .sqlite. C'est une habitude à prendre, car au final la base de données, ce sont les données (Spatialite) et son interface (projet QGIS). Du coup, dès que la base de données doit être déplacée, copiée, transférée, etc., ce sont les deux fichiers (.sqlite et .qgz), avec le **même nom et au même emplacement** qui bougent.
![](images/2_fichiers.png)



## 3.3. Une base de données vierge ?

QGIS a donc créé une base de données Spatialite tout seul comme un grand. Il n'y a aucune table ou données structurées. La base de données est donc vierge...
Mais est-ce vraiment le cas ?
* Installer **DB Browser for SQLite**. Il s'agit d'un logiciel gratuit et libre qui permet de créer et de manipuler des bases de données SQLite. Parmi les quelques logiciels faisant la même, son interface reste ergonomique et intuitive. Il existe même une version portable de l'application qu'on peut laisser sur une clé USB par exemple. On peut télécharger le logiciel [ICI](https://sqlitebrowser.org/dl/).
![](images/dbbrowser_install.png)
* Ouvrir ![logoDB](images/logoDB.png)DB Browser for SQLite.
* Ouvrir la base de données **SARAN.sqlite**. 
![](images/ouvrir_dbbrowser.png)

On constate que la base de données créée par QGIS n'est pas "vide". Elle possède déjà des tables structurées (22). En fait, lors de la création de la base de données, QGIS ajoute lui-même (avec ses petits bras musclés :muscle: ) la composante spatiale. Assez pratique, puisqu'on n'a pas à le faire soi-même. C'est donc bien une base de données Spatialite qui a été créée.
![](images/table_dbbrowser.png)

* Montrer que les tables sont structurées. Elles contiennent des champs. Et surtout de la donnée. Par exemple la table *spatial_ref_sys* contient les 6215 SCR référencés par le moteur de base de données à ce moment.
* Laisser ![logoDB](images/logoDB.png)DB Browser for SQLite ouvert, on pourra y revenir ultérieurement pour constater les évolutions de la base de données SARAN.



## 3.4. Import de tables et données

* **Retour dans QGIS**.
* Bien vérifier les SCR et les encodages sur les couches chargées dans le projet.
* Commencer avec la couche **F108633_fait**.
* Lors des vérifications, on s'aperçoit que les champs de cette couche ne sont pas tous typés correctement. Il suffit d'ouvrir les propriétés de la couche pour s'en rendre compte. On constate par exemple que le champ "num_fait" est typé en *double* c'est à dire en *réel*. Il faudrait qu'il soit en *entier*. Il faut donc **refactoriser** ce champ. 
  * Dans QGIS, ouvrir la Boîte à outils![](images/toolbox.png) et taper dans la barre de recherche "refact", on obtient dans la liste la fonction ![](images/refactoriser.png).
  * :one:Dans la nouvelle fenêtre, sélectionner F108633_fait dans *Couche source*.
  * :two:Dans *Correspondance des champs*, il faut passer le champ "num_fait" en **Entier** dans la colonne *Type*.
  * :three:Pour *Couche refactorisé*, on nous précise que QGIS va produire une couche temporaire. C'est cette couche temporaire refactorisée que l'on importera dans la base de données.
  * :four:Faire *Exécuter*.
  * Fermer la fenêtre.
  * *Couche refactorisée* a été ajoutée dans le panneau Couches. On peut renommer cette couche **fait_refact**.

![](images/refact_champs.png)

* Ouvrir le **Gestionnaire de base de données**![icon GS](images/iconDB.png). Ce module sert entre autres choses à administrer des bases de données : création de table, modification, suppression... Il permet aussi l'accès à toutes les sources de données.
* On peut aussi ouvrir le gestionnaire de base de données en passant par le menu *Base de données*.
![](images/menu_DBmanager.png)
* On peut détailler la fenêtre du *Gestionnaire BD*
  * :one:A gauche, on trouve un panneau qui rassemble les couches/tables auxquelles on peut avoir accès. Elles peuvent provenir de bases de données PostGIS ou bien sûr Spatialite, mais on a également accès au couches déjà présentes dans le projet QGIS courant. Il suffit de développer *Couches virtuelles* puis *Couches du projet*.
  * :two:La partie permet de visualiser la donnée une fois qu'une couche/table a été sélectionnée à gauche. On trouve trois onglets. L'onglet *Info* décrit la couche (nombre d'entités, type de géométrie s'il y en a une, SCR et emprise, liste des champs avec leurs typages...). L'onglet *Table* permet de visualiser la table attributaire de la couche/table sélectionnée. L'onglet *Aperçu* permet d'observer la géométrie de la couche/table sélectionnée (NB : si une symbologie est associée par défaut à la couche, elle sera visible dans l'onglet *Aperçu*).
  * :three:Une barre d'outil, classique dans l'environnement QGIS. Un bouton *Actualiser*![](images/actualiser.png) qui permet de rafraichir les gestionnaire après une mise à jour à une modification d'un donnée. Un bouton *Fenêtre SQL*![](images/fenetreSQL.png) qui permet d'ouvrir un nouvel onglet dans la partie de droite : c'est ici que l'on pourra interroger la donnée en utilisant le langage SQL. Un bouton d'importation![](images/import.png) qui permet d'importer, dans une base de données, une table structurée avec sa donnée (c'est ce que l'on va faire de suite). Et un bouton d'exportation![](images/export.png) qui permet d'exporter des tables d'une base de données dans d'autres format (comme le shp par exemple).

![](images/fenetre_DBmanager.png)


* Dans le panneau de gauche, développer ***Spatialite***. On doit voir la base SARAN.
* Développer SARAN.sqlite. Le *Gestionnaire BD* est alors connecté à la base de données.
* Clic sur le bouton d'**importation**![](images/import.png). Un nouvelle fenêtre apparaît. Elle se nomme *Importer une couche vecteur*. Elle permet de créer une table dans la base en se basant sur une structure existante, tout en important également la donnée.
* Commencer tout d'abord par importer la table des *faits* dans la base de données SARAN.
* Comme à chaque fois, on prend les choses dans l'ordre.
  * :one:Dans *Source*, choisir la couche à importer. Commencer donc par la couche **fait_refact**, la version refactorisée de F108633_fait.
  
  * Si une sélection était faite sur la couche en question dans l'environnement QGIS, on pourrait décider d'importer uniquement la donnée selectionnée en cochant *Importer uniquement les entités sélectionnées*.
  
  * Table en sortie *Schéma* : ce n'est pas une option disponible avec une base de données Spatialite. Ceci est propre aux base de données PostgreSQL. Un schéma est niveau structurel sous-jacent de la base. Pour faire simple : un schéma contient un groupe de tables, une base de données contient un groupe de schémas.
  
  * :two: Table en sortie *Table* : indiquer ici le nom de la nouvelle table de la base de données. Comme toujours, le plus court et le plus explicite possible. Inutile dans ce cas de rappeler le numéro SGA puisque la table est contenue dans la base qui, elle, pourra inclure le numéro SGA. Il est également souhaitable et conseillé d'**ajouter un préfixe au nom en fonction de la nature de la table**. On le verra, il peut y avoir plusieurs types de tables dans une base de données : des tables "utiles" (saisie, consultation...), des tables de jonction (relation de *n* à *n*), ou encore des vues SQL. On propose d'utiliser le préfixe "t" pour les tables "utiles". Donc, nommer la table **t_fait**.
  
  * :three:Même si on est sûr que la table de fait possède un identifiant unique, le numéro de fait (champ "num_fait"), on conseille ici de laisser le logiciel gérer sa propre clé primaire, comme vu plus haut. On évite tout problème par la suite. Laisser la clé primaire **id**.
  
  * :four:Dans *Colonne géométrique*, c'est le nom du champ qui stocke la valeur représentant la géométrie de l'entité. Dans tous les cas, on touche à rien. Si la table source n'a pas de géométrie, pas de souci. Si elle en possède une, on laisse le nom du champ **"geom"**.
  
  * :five:En ce qui concerne le SCR, il est évident qu'on ne touche à rien. On laisse **2154**
  
  * :six:Pour l'encodage, on ne touche à rien.
  
  * Au cas où l'on veut "écraser" une table qui existe déjà, on coche *Remplacer la table de destination (si existante)*.
  
  * On peut également décider d'interdire les entités multipartites dans la future table en cochant *Ne pas prrmouvoir en multi-partie*.
  
  * Enfin, un *index spatial* permet de rendre les requêtes spatiales plus rapides, dans le cas où la table contient ou contiendra un nombre très conséquent d'entités.
  
  * Valider en cliquant sur **OK**.
  
    

![](images/import_table_DBmanager.png)



Si tout va bien, un message de réussite apparaît ↓

![](images/import_OK.png)

La table t_fait est apparue dans la base de données. Si ce n'est pas le cas, vérifier que la base est sélectionnée et Actualiser![](images/actualiser.png).
Dans l'onglet *Info*, le champ qui sert de clé primaire est souligné.


* **Faire de même avec les tables suivantes** en étant vigilant sur le typage des champs avant l'importation, il faudra peut-être refactoriser les couches :
  * F108633_us :arrow_right: **t_us**
  * tabPhoto :arrow_right: **t_photo**
  * tabLienPhoto :arrow_right: **j_photo**, on utilise le préfixe "j" pour préciser dans la structure qu'il s'agit d'une table de jonction permettant d'établir une relation de *n* à *n* entre les photos et les sujets des photos.

![](images/tables_bdd.png)


Dans DB Browser for SQLite, on remarque également la présence des tables nouvellement créées (faire F5 pour rafraîchir si besoin).

* Dans QGIS, nettoyer le projet : **supprimer les couches** qui ont servi de source pour structurer les tables de la base de données SARAN : F108633_fait, F108633_us, tabPhoto et tabLienPhoto.
* **Charger les nouvelles tables dans le projet**. Dans le panneau Explorateur, développer Spatialite, puis SARAN.sqlite. Ajouter les couches **t_fait**, **t_us**, **t_photo** et **j_photo**.

***Remarque*** : il peut très bien coexister dans le même projet QGIS des sources de données différentes, cela ne pose aucun problème.

![](images/divers_couches.png)


## 3.5. Créer une table vierge

On va créer une table qui nous pourra nous permettre d'enregistrer les différentes hypothèses de regroupement de vestiges (ensemble, bâtiment, enclos...).
* Ouvrir le **Gestionnaire de base de données**![icon GS](images/iconDB.png).  
* Sélectionner la base SARAN (permet la connexion au fichier).
* Menu **Table**, choisir **Créer une table**. Une nouvelle table apparaît.
![](images/creer_table.png)
* :one:Nommer la nouvelle table : ***t_interpret***.
* :two:Ajouter les champs **id** (integer), **numinterp** (integer), **typinterp** (text) et **descrip** (text).
* :three:Choisir la clé primaire : **id**.
* :four:Cette table une géométrie polygonale, permettant par exemple de dessiner des bâtiments. Choisir **MULTIPOLYGON**.
* :five:Ne pas changer le nom de la colonne géométrique **geom**.
* :six:Laisser en deux dimensions (XY).
* :seven:Le SRID (Spatial Reference Identifier) correspond à l'EPSG (European Petroleum Survey Group). Choisir **2154**
* :eight:Terminer en cliquant sur **Créer**.
* Logiquement, le message "La table a été créée avec succès" apparaît. Valider par **OK**.

![](images/fenetre_creer_table.png)





:warning: :warning: **Il est fortement recommandé, à partir de ce moment, où les tables sont créées dans la base de données, de repartir sur un projet QGIS *vierge* afin d'éviter les occurrences de tables intempestives et indésirables, notamment dans la liste des couches/tables disponibles pour définir les relations dans un projet.** :warning: :warning:



## 3.6. Création des relations dans le projet QGIS

Où formalise-t-on les relations entre les tables pour la base de données ? 
Il est tout à fait possible de se servir de DB Browser for SQLite pour définir des champs comme clés étrangères![fk](images/icon_fk.png). Cela modifie directement le code SQL de la base de données. Il faut alors être prudent car il peut y avoir des "conflits" de clés empêchant le bon fonctionnement de la base de données.
Mais, dans le cas où, comme ici, nous nous servons de QGIS comme interface d'administration, de saisie, de consultation et d'interrogation, "écrire" les relations directement dans le code SQL de la base n'a que peu d'intérêt. En effet, il faudra de toute façon le refaire dans QGIS. Donc, autant ne le faire qu'une seule fois, au niveau du projet / interface.
Car, on le rappelle, **les relations entre les couches ou tables sont créées et gérées au niveau du projet**. 


### 3.6.1. Les relations de 1 à 1

On en reverra ici les étapes de mise en place d'une relation de 1 à 1, puisqu'il s'agit tout simplement de réaliser une jointure, tel qu'on le voit dans les formation [Niv. 1 Module découverte et Niv. 2 Les figures du rapport avec QGIS](https://formationsig.gitlab.io/toc/#sig-31-bases-de-donnees-spatiale-et-attributaire).


### 3.6.2. Les relations de 1 à *n*

On reprend l'exemple de la relation entre les faits et les US. On peut s'aider du Modèle Logique de Données (MLD) tracé en début de formation.

![](images/fait_us.png)

* Aller dans le menu ***Projet*** et choisir le sous menu ***Propriétés***.

![](images/proprietes_projet.png)

* Sur la fenêtre de propriétés du projet, aller à l'onglet ***Relations***.
* La fenêtre affiche la liste des relations existantes du projet. En toute logique, il n'y en a aucune. Pour cela, il faut en ajouter une en cliquant sur ![](images/ajouter_relation.png).
* Une nouvelle fenêtre apparaît. Elle permet de paramétrer la relation.
  * :one:Si on ne met rien dans ***Id***, QGIS va générer automatiquement un long numéro (uuid) peu pratique. Si, à terme, on souhaite développer des formulaires via le module QT Designer fourni avec l'installation de QGIS, c'est cet *id* que l'on mentionnera pour utiliser la relation, donc autant se faciliter la vie. On conseille ici de mettre la même valeur que dans *Nom*.
  * :two:***Nommer la relation***. On peut prendre l'habitude de nommer la relation par la concaténation de, en premier lieu, la table mère (*t_fait*), avec, en deuxième lieu, la table fille (*t_us*). Ainsi, on nomme la relation **fait_us**.
  * Pour l'option *Force de la relation*, laisser ***Association***. L'option' par défaut *Association* signifie que la couche parent est simplement liée à la couche enfant alors que le type *Composition* vous permet de dupliquer les entités de la couche enfant lorsque vous dupliquez celles de la couche parent.
  * :three:Il s'agit d'indiquer quelle est la table référencée c'est à dire la parent, ou table mère. Donc choisir **t_fait**.
  * :four:Mettre ici la table référençante, c'est à dire la table enfant ou table fille. Choisir **t_us**.
  * :five:Choisir la clé primaire![pk](images/icon_pk.png) de la table mère  *t_fait*, à savoir **"num_fait"**.
  * :six:Choisir la clé étrangère![fk](images/icon_fk.png) de la table fille *t_us*, à savoir **"num_fait"**
  * :seven: Cliquer sur ***OK***.

![](images/parametres_relation.png)

A partir de là, une relation est présente dans la liste. Contrairement à une jointure classique (relation de 1 à 1 sur une couche) la "relation" n'est pas modifiable. Seule le nom de la relation peut éventuellement être changé. Si une erreur a été fait (choix des tables ou clés), il faut supprimer la relation![](images/supprimer_relation.png) et la refaire correctement en ![](images/ajouter_relation.png).

* Valider les propriétés du projet et fermer la fenêtre en cliquant sur **OK**.

On peut d'ores et déjà aller vérifier si la relation a été correctement prise en compte par QGIS et répercutée sur les couches correspondantes (*t_fait* et *t_us*).

* Ouvrir la **table attributaire** de la table *t_fait*.
* Appuyer sur *Basculer sur la vue formulaire*![](images/form_ou_tab.png).
* Dans le panneau de gauche, si ce ne sont pas les numéros de fait qui sont listés, changer en cliquant sur *Expression*, puis *Prévisualisation de colonne*, puis sélectionner le champ *"numfait"*.
* Sélectionner, par exemple, le fait F.76.
* Dérouler vers le bas le formulaire de droite.

On constate alors que la relation est prise dans le formulaire par défaut de la table *t_fait*. La table fille est en effet présente avec seulement les US en relation avec le fait. Il est également possible de passer facilement du formulaire à la table attributaire en cliquant sur *Basculer sur la vue formulaire* ou sur *Basculer sur la vue tabulaire* ![form_ou_tab](images/form_ou_tab.png).

![](images/form_fait_simple.png)

* Fermer la table attributaire de la table *t_fait*.

:warning: Parfois, lors de la création de relation, QGIS propose 2 fois la table *t_fait*. Il s'agit d'un bug, quelque fois dû à des erreurs de création de table dans la base de données. Si la mauvaise version de la table est choisie pour la relation, cela peut engendrer un disfonctionnement : défaut d'affichage des US liées dans la table des faits. Dans ce cas, il fait refaire la relation et choisir la deuxième proposition de *t_fait*. Ou mieux, depuis le Gestionnaire BD![db](images/iconDB.png), il est possible de supprimer la mauvaise table (la table temporaire, en passant *Couches du projet*).

> Note: les versions récentes de QGIS reconnaissent les relations 1 à n des bases de données SQLite (et PostgreSQL), il est donc possible de paramétrer les relations directement dans ![db](images/logoDB.png)DB browser for SQLite c'est à dire de définir les clés étrangères. 
>
> :warning: dans l'exemple ci-dessous les noms de tables sont légèrement différents... à reprendre :construction_worker:
>
> * Ouvrir la base de données dans DB browser for SQLite
>
> * Chercher la table **tabUS** et faire un clic droit →  Modifier une Table
>
>     ![dbbrowser_modif_table](images/dbbrowser_modif_table.png) 
>
> * Définir la clé étrangère ![fk](images/icon_fk.png) en spécifiant pour le champ référençant ("num_fait"), la table mère (**tabFait**) puis le champ de référence dans celle-ci ("num_fait") . :warning: Attention il faut absolument valider chaque étape en validant avec la touche [entrée] puis le bouton [OK].
>
>   ![dbbrowser_cle_etrangere](images/dbbrowser_cle_etrangere.png)
> * On peut alors vérifier dans QGIS que la relation a bien été prise en compte depuis Menu Projet → Propriétés.. → Relations
>
> ![dbbrowser_qgis_decouverte_relation](images/dbbrowser_qgis_decouverte_relation.png)

### 3.6.3. Les relations de *n* à *n*

Prenons le cas des faits et des photos pour travailler sur la relation de *n* à *n*. On peut s'aider du Modèle Logique de Données (MLD) tracé en début de formation ou complété à ce moment.

![](images/fait_photo.png)


:runner:**Une fois le schéma si dessus bien compris, les relations sont à réaliser en *autonomie* par les stagiaires** :runner: . 

:pencil:***Correction et explication.***
Il y a donc deux relations à rajouter au projet pour faire fonctionner la relation entre les faits et les photos.

* Menu **Projet**. Puis **Propriétés**.
* Onglet **Relations**.
* Commencer par la relation de 1 à *n* entre la table des faits *t_fait* et la table de jonction *j_photo*. Cliquer sur **Ajouter une relation** ![](images/ajouter_relation.png).
* Nommer la relation par un nom clair et simple, par exemple *fait_jonction*. La couche référencée (parent) est la table *t_fait* et sa clé primaire![pk](images/icon_pk.png) le champ *"num_fait"* ; la couche référençante (enfant) est la table *j_photo* avec sa clé étrangère![fk](images/icon_fk.png) le champ *"fait"*.
![](images/fait_jonction.png)
* Valider par **OK**.
* Continuer par la relation de 1 à *n* entre la table des photos *t_photo* et la table de jonction *j_photo*. Cliquer sur **Ajouter une relation** ![](images/ajouter_relation.png).
* Nommer la relation par un nom clair et simple, par exemple *photo_jonction*. La couche référencée (parent) est la table *t_photo* et sa clé primaire![pk](images/icon_pk.png) le champ *"photo"* ; la couche référençante (enfant) est la table *j_photo* avec sa clé étrangère![fk](images/icon_fk.png) le champ *"photo"*.
![](images/photo_jonction.png)
* Valider par **OK**.

Dans l'onglet Relations des Propriétés du Projet, on a donc trois lignes de relations définies : une pour la relation de 1 à *n* entre les faits et les US et deux lignes pour la relation de *n* à *n* entre les faits et les photos (en passant par une table intermédiaire).

Si on ouvre à nouveau la table attributaire de la table *t_fait* et que l'on visualise le formulaire d'un fait, par exemple F.76, on observe la présence des deux relations créées pour la table *t_fait*. Pas de souci pour la liste des US, on l'a vu plus haut. Par contre, on observe la relation vers la table de jonction vers les photos, mais nous n'avons, en l'état, pas  d'information sur la photo. 

![](images/form_fait_simple_photo.png)

** :angry: TOUT CELA EST ASSEZ DONC PEU ERGONOMIQUE :rage: **





# 4. Création de formulaires

Le but est de profiter des fonctionnalités de QGIS : ***style***, ***Conception par glisser/déposer*** et ***Thème de carte***.

## 4.1. Un formulaire simple

Commencer par un formulaire très simple pour la table des faits *t_fait*.

* Clic droit la couche/table *t_fait*. Styles puis Ajouter... <img src="images/boom.png" style="zoom:50%;" />**Le Style :bangbang:**
* Nommer le style dans la fenêtre *Nouveau Style* : **formulaire simple**. 
![](images/nouveau_style_form_simple.png)
* Aller sur les **propriétés de la couche** *t_fait*. Deux onglets permettent d'afficher et de gérer les champs de la couche : *Champs* et *Formulaire d'attributs*.
* Aller sur l'onglet ***Formulaire d'attributs***. Il permet de gérer le comportement et l'affichage (ou pas) de chaque champ, de paramétrer les outils d'édition de ces champs, mais également de les organiser en onglets ou groupes... Par défaut, le formulaire est en *Génération automatique*. Chaque champ est déjà, en l'état, paramétrable. En cliquant dessus un certain nombre d'options apparaît à droite.
* Choisir ***Conception par Glisser/déplacer***. Un nouveau panneau apparaît au centre de la fenêtre : *Disposition du formulaire*. On y voit tous les champs intégrés par défaut au formulaire.
![](images/conception_glisser_deplacer.png)
* En cliquant sur un champ, à gauche ou au centre, vous avez accès à droite au propriétés d'édition.
* Choisir **"num_fait"**.
* Dans les propriétés à droite, il y cinq groupes : ***Affichage***, ***Général***, ***Type d'outil***, ***Contraintes*** et ***Défauts***.
![](images/outil_edition.png)
* Dans *Affichage*, on peut choisir de montrer l'étiquette du champ ou pas.
* Dans *Général*, il est possible de donner un nom plus "lisible" au champs...
* Dans *Type d'outil*, le comportement du champ peut être changé. La liste des types d'outil de champ est la suivante : 
  * **Case à cocher** : affiche des cases à cocher et rempli les champs en fonction des valeurs "Etat coché" et "Etat non coché".
  * **Classification** : on ne peut l'utiliser que si une symbologie catégorisée est appliquée à la couche. Il affiche une liste de valeurs qui correspond aux catégories définies.
  * **Couleur** : permet de sélectionner une couleur qui sera converti en code html. C'est très pratique si, pour une symbologie, on utilise une couleur définie par les données.
  * **Date/Heure** : s'applique à un champ de type texte. Affiche un widget pour sélectionner la date. Permet également de choisir le format de la date qui deviendra la valeur. On privilégiera le format année / mois / jour (YYYY-MM-DD) qui permet un tri chronologique efficace.
  * **Enumération** : sorte de liste de valeurs, gérée uniquement par PostgreSQL.
  * **Pièce jointe** : utilise une boîte de dialogue « Ouvrir un fichier » pour stocker le chemin du fichier en mode relatif ou absolu. Il peut également être utilisé pour afficher un lien hypertexte (vers le chemin du document), une image ou une page Web.
  * **Cachée** : le champ et sa valeur seront cachés.
  * **Clé/Valeur** : pris en charge par PostgreSQL. Cela affiche un tableau à deux colonnes pour enregistrer des paires clé/valeur.
  * **Liste** : pris en charge par PostgreSQL. Sorte de rubrique multivaluée.
  * **Plage** : permet de spécifier une plage de valeurs numériques avec une valeur minimale et une valeur maximale. On peut l'afficher sous forme de barre coulissante par exemple.
  * **Référence de la relation** : si une relation est définie dans le projet, cet outil permet de référer la clé étrangère de la couche courante (fille) vers la clé primaire de la couche référencée (mère). Avec des widgets pour afficher les formulaires...
  * **Edition de texte** : c'est l'outil par défaut. On édite la valeur manuellement.
  * **Valeurs Uniques** : sélection de l’une des valeurs déjà utilisée dans la table attributaire. 
  * **Générateur d'Uuid** : il génère un identifiant unique géré par le logiciel. Non éditable. Peu pratique.
  * **Liste de valeurs** : liste déroulante de valeurs saisies manuellement ou encore depuis un fichier CSV.
  * **Valeur relationnelle** : En sélectionnant une table/couche de référence, des colonnes clés et valeur (genre un Thésaurus), permet d'affiche une liste déroulante. Les solutions multiples peuvent être autorisées...
  ![](images/type_outil.png)

Commençons à mettre en forme le formulaire par Glisser/déplacer de la table *t_fait*, en partant pour l'exemple d'une page vierge.
* Dans la partie centrale de la fenêtre, sélectionner tous les champs et supprimer les du formulaire en cliquant sur ![](images/moins.png).
* On va ajouter les champs au fur et à mesure tout en les mettant en forme.
* Le champ "id" n'a pas besoin d'être ajouté dans le formulaire car il s'agit de la clé primaire ![pk](images/icon_pk.png) de la table *t_fait*. Son incrémentation automatique est gérée par QGIS, tout comme son caractère UNIQUE et NON NULL. D'ailleurs, on constate que ce champ est déjà paramétré.
* Ajouter le champ "num_fait" par Glisser/déposer. Donner lui un **Alias**. Nommer le **Fait**. Comme il s'agit ici du numéro de fait, sa saisie devrait être obligatoire et sans doublon : cliquer sur **"Non nul"** et **"Unique"**.
* Ajouter le champ "typoly". Alias : **Type**. Comme on attend ici comme valeur soit 'fait' soit 'annulé', il faut normaliser la saisie. Dans Type d'outil, choisir **Liste de valeurs**. Pour cette base de données, on peut se servir de la donnée existante. Cliquer sur **Charger des données depuis la couche**. Et récupérer les valeurs déjà saisies du champ "typoly" de la couche *t_fait*. La *Valeur* est le texte saisi dans le champ, la *Description* peut correspondre à un texte différent qui sera affiché (mais pas saisi). Cliquer également sur *Insérer la valeur NULL au-dessus*. Puis **OK**.
![](images/liste_valeurs_depuis.png)
* Tester les modification du formulaire en allant identifier par exemple une entité (après avoir valider et fermer la fenêtre de propriétés).
* En fonction de la version de QGIS, l'identification d'une entité en passant par ![](images/identifier.png), peut être problématique car QGIS interprète mal le fait qu'il y ait des entités sans géométrie (et c'est le cas ici). Donc, il faut le contraindre, au niveau de la symbologie (et donc du canevas) de ne considérer graphiquement que les entités AVEC une géométrie. Aller dans les propriétés de la couche t_fait, onglet **Symbologie**. Passer en *Ensemble de règles*. Et dans la règle, entrer la requête suivant: 
```sql 
$geometry is not null
```
L'identification ne concerne maintenant que les entités qui ont une géométrie (avec une géométrie non nulle).
* Tester les modification du formulaire en allant identifier par exemple une entité. 
![](images/t_fait_2champs.png)

On a alors un formulaire très simple avec les deux champs, très pratique pour la numérisation, puisqu'on a juste les champs minimum et nécessaires pour la validation de l'entité.

## 4.2. Un formulaire complet

En continuant à jouer avec les Styles, sur la couche t_fait, il est possible de créer un autre formulaire, plus détaillé cette fois-ci.

* Clic droit la couche/table *t_fait*. Styles puis Ajouter... <img src="images/boom.png" style="zoom:50%;" />**Le Style :bangbang:**
* Nommer le style dans la fenêtre *Nouveau Style* : **formulaire détaillé**. 
![](images/nouveau_style_form_detail.png)
* Revenir sur les propriétés de la table *t_fait*, onglet *Formulaire d'attributs*.
* Ajouter un conteneur (**Onglet ou groupe**) en appuyant sur ![](images/plus.png). Nommer le **Interprétation**. Le premier conteneur ajouté est obligatoirement un *Onglet*.
![](images/conteneur_interpret.png)
* Dans cet onglet, glisser/déposer les champs **"ident"**, **"ident_alte"**, **"ident_dout"**, **"statut"**, **"fouille"**, **"descrip"** et **"datation"**.  Avec les paramètres suivants :
  * "ident" : alias **Identification**. Type d'outil : Liste de valeurs, en récupérant les valeurs déjà saisies dans le champ "ident" de la table *t_fait* (REMARQUE : pour les liste de valeurs, il est préférable d'exploiter une table à part et structurée servant de thésaurus ; dans ce cas, on utilisera le type d'outil *Valeur relationnelle*).
  * "ident_alte" : alias **ou**. Type d'outil : idem.
  * "ident_dout" : alias **Douteux**. Type d'outil : Case à cocher ; Etat coché : 1 ; Etat non coché : 0.
  * "statut" : alias **Statut**. Type d'outil : Liste de valeurs ('fouillé', 'non fouillé', 'annulé' et Ajouter une valeur NULL).
  * "fouille" : alias **Fouillé à**. Type d'outil : Liste de valeurs ('50 %', '100 %', 'par sondage' et Ajouter une valeur NULL).
  * "descrip" : alias **Description**. Type d'outil : Edition de texte ; Multi-ligne.
  * "datation" : alias **Datation**. Type d'outil : Edition de texte.
* Tester les modification du formulaire en allant identifier une entité. 
![](images/formulaire_detail.png)

On peut organiser davantage le formulaire.
* Revenir sur les propriétés de la table *t_fait*, onglet *Formulaire d'attributs*.
* Ajouter un conteneur ![](images/plus.png). Nommer le **Identification**. Choisir *un groupe dans un conteneur* et spécifier Interprétation. Nombre de colonnes : 3. Valider par OK.
* Glisser/déplacer les champs "ident", "ident_alte" et "ident_dout" dans le groupe nouvellement créé. 
* Déplacer ce groupe en haut dans l'onglet *Interprétation*.
* Ajouter un conteneur ![](images/plus.png). Nommer le **Traitement**. Choisir *un groupe dans un conteneur* et spécifier Interprétation. Nombre de colonnes : 2. Valider par OK.
* Glisser/déplacer les champs "statut", "fouille" dans le groupe nouvellement créé.
* Déplacer ce groupe sous le groupe Identification.
* Tester les modification du formulaire en allant identifier une entité.
![](images/formulaire_detail_interpret.png)
* Revenir sur les propriétés de la table *t_fait*, onglet *Formulaire d'attributs*.
* Ajouter un nouvel onglet ![](images/plus.png). Nommer le **Dimensions et morphologie**. Nombre de colonnes : 2.
* Ajouter un conteneur ![](images/plus.png). Nommer le **Dimensions**. Choisir *un groupe dans un conteneur* et spécifier Dimensions et morphologique. Nombre de colonnes : 1. Valider par OK.
* Glisser/déplacer les champs "diam", "long", "larg", "prof", "haut" et "epaiss" dans le groupe nouvellement créé. 
* Donner leur des jolis alias. Ces champs peuvent rester en Type d'outil Edition de texte.
* Ajouter un conteneur ![](images/plus.png). Nommer le **Morphologie**. Choisir *un groupe dans un conteneur* et spécifier Dimensions et morphologique. Nombre de colonnes : 1. Valider par OK.
* Glisser/déplacer les champs "forme", "profil" et "orient" dans le groupe nouvellement créé. 
* Donner leur des jolis alias. Ces champs peuvent passer en Type d'outil Liste de valeurs (ou Valeur relationnelle si la base est dotée d'une table/thésaurus).
* Tester les modifications du formulaire en allant identifier une entité.
* Revenir sur les propriétés de la table *t_fait*, onglet *Formulaire d'attributs*.


## 4.3. Afficher les relations dans les formulaires

Il est temps de s'occuper des relations.
Commençons avec la relation vers les US (relation de 1 à *n*).
* Ajouter un nouvel onglet ![](images/plus.png). Nommer le **US du fait**.
* Glisser/déplacer la relation ***fait_us***. Décocher *Montrer l'étiquette*.
* Tester les modifications du formulaire en allant identifier une entité.
* Dans l'onglet US du fait, on observe la liste des US enregistrées et associées au fait identifié (selon la relation de 1à *n* définie par la base de données et le projet). 
* :warning: IMPORTANT​ :warning: Si un "joli" formulaire a été créé dans les propriétés de la table t_us, il sera embarqué dans le formulaire des faits qui exploite la relation.

Continuons avec la relation entre les faits et les photos (relation de *n* à *n*).
* Revenir sur les propriétés de la table *t_fait*, onglet *Formulaire d'attributs*.
* Ajouter un nouvel onglet ![](images/plus.png). Nommer le **Photos**.
* Glisser/déplacer la relation ***fait_jonction***. Décocher *Montrer l'étiquette*. En l'état, si on en reste là, on retrouvera dans le formulaire de la table *t_fait* le sous-formulaire de la table *j_photo*. OR, et c'est là que réside la magie, QGIS est capable de suivre la relation vers la table *t_photo*, car il connaît les deux relations de 1 à *n*. 
* Dans Cardinalité, "Relation un à plusieurs" est sélectionné par défaut. Choisir **t_photo(photo)**. 
![](images/cardinalité_t_photo.png)
* Tester les modification du formulaire en allant identifier une entité.
* Dans l'onglet Photo, on observe la liste des Photos enregistrées et associées au fait identifié (selon la relation de *n* à *n* définie par la base de données et le projet). 
* :warning:IMPORTANT :warning: Si un "joli" formulaire a été créé dans les propriétés de la table t_photo, il sera embarqué dans le formulaire des faits qui exploite la relation.



## 4.4. Le contrôle de visibilité

Restons sur la table *t_fait* et discutons du cas des faits annulés. Si un fait est annulé, nous n'avons pas besoin d'afficher les différents onglets.
* Revenir sur les propriétés de la table *t_fait*, onglet *Formulaire d'attributs*.
* Sélectionner le groupe **Interprétation**.
* Cocher *Contrôle de visibilité par expression*. On va écrire une expression SQL qui précisera à quel condition l'onglet sélectionné doit apparaître.
* En passant par le calculateur d'expressions, écrire la requête suivante : 
```sql 
 "typoly" NOT LIKE 'annulé'
```
![](images/controle_visibilite.png)
* Faire de même pour les onglets **Dimensions et morphologie**, **US du fait** et **Photos**.
* Tester les modification du formulaire en ouvrant la table attributaire de la table *t_fait*. 

Ces "masques de saisie" ouvrent des possibilités d'adaptation des formulaires en fonction de nombreux critères. Par exemple, on peut imaginer des onglets particuliers dans le cas de faits interprétés comme sépulture.


:clock1:**Dans l'idéal, c'est la fin de la première journée**​ :clock1:




# EXERCICE

En :running: autonomie​ :running:, intégrer le **mobilier** dans la base de données (1 heure max.).
* Créer la table *t_mob* à partir du fichier InvMob.xls (2_SARAN_BRUT/Tableur) dans la base de données. 
* Discussion sur la question de la géométrie. Initialement la table de mobilier n'a pas de géométrie, mais il est possible d'ajouter la possibilité d'en avoir lors de l'importation de la couche dans la base de données (POINT). Question de modélisation.
* Créer la relation entre les US et le mobilier (Propriétés du projet).
* Créer la relation entre les faits et les ensembles (t_interpret).
* Développer de l'ergonomie pour les tables *t_us*, *t_mob*, *t_photo* et *t_interpret*. Ne pas oublier de déclarer des styles <img src="images/boom.png" style="zoom:50%;" />**Le Style :bangbang:**.
* Discussion sur la relation entre la table **t_interpret** et les tables **t_us** et **t_fait**. S'il s'agit d'une relation de 1 à *n*, il faudra ajouter les clés étrangères![](images/icon_fk.png) dans les tables filles ; s'il s'agit d'une relation de *n* à *n*, il faudra ajouter une table intermédiaire qui gérera les "couples relationnels". Question de modélisation.

:point_right: Propriétés de **t_us**, formulaire d'attributs.

![](images/t_us_proprietes.png) 

![](images/t_us_form.png)



:point_right: Propriétés de **t_mob**, formulaire d'attributs.

![](images/t_mob_proprietes.png) 

![](images/t_mob_form.png)



:point_right:Propriétés de **t_photo**, formulaire d'attributs.

![](images/t_photo_proprietes.png) 

![](images/t_photo_form.png)



:point_right: Propriétés de **t_interpret**, formulaire d'attributs.



![](images/t_interpret_proprietes.png) 

![](images/t_interpret_form.png)


Prendre un moment pour parcourir l'ergonomie mise en place par la définition des relations et la création des formulaires.



## 4.5. Thèmes de carte

Rappel de la formation [Les figures du rapport avec QGis](https://formationsig.gitlab.io/toc/#sig-2--les-figures-du-rapport-avec-qgis) : Le ***Thème de carte*** conserve les couches cochées dans l'arborescence ainsi que le Style de couche associé au moment où le thème est enregistré.
Tout comme la symbologie ou les étiquettes, les propriétés d'un formulaire d'attributs sont enregistrées dans le style de la couche.
Par conséquent, en enregistrant un **Thème de carte**, les formulaires définis des couches seront stockés dans le thème.

* Cliquer sur l'oeil![oeil](images/oeil.png) du panneau *Couches*.
* ***Ajouter un thème***.
* Nommer le **Formulaire détaillé**.
* ![](images/ajout_theme.png)

La gestion de plusieurs Thèmes de carte permet de concevoir pour la même base de données des ergonomies différentes, plus ou moins détaillées, avec tout ou partie des champs...


## 4.6. La question des images

Dans la table **t_photo**, il y a au moins deux champs relatifs aux noms de fichier et à leurs chemins d'accès, permettant ainsi de faire le lien vers la photo. 
Il s'agit du champ **"fichier"** qui stocke les valeurs correspondant aux noms de fichier des photos et le champ **"chemin"** qui stocke le chemin d'accès absolu de la photo.
:warning:Les valeurs contenues dans le champ "chemin" ne sont pas les bonnes. Elles ne correspondent pas à l'arborescence de l'utilisateur.
Il faut mettre à jour le champ "chemin".

* Ouvrir la table attributaire de la table **t_photo**.
* Activer le **Mode Edition**.
* Ouvrir la **Calculatrice de champ**.
* Saisir l'expression suivante :
```sql
 @project_folder ||'\\Photo\\'|| "fichier" 
```

@project_folder est une variable qui permet de récupérer le chemin d'accès du projet en cours.

Remarque : le chemin d'accès pourrait également être stocké dans un champ virtuel, ce qui rendrait le lien dynamique.

* Faire **OK**.
* Enregistrer les modifications et sortir du Mode Edition.
* Fermer la table attributaire et ouvrir les propriétés de la table t_photo.
* Onglet Formulaire d'attributs.
* Sélectionner le champ "chemin".
* Dans *Type d'outil*, choisir **pièce jointe**.
* Dans *Visualisateur de document interne*, choisir **Image**. Il est possible de spécifier la tailler du cadre d'image. On peut laisser sur *Auto*. 
* Tester les modifications du formulaire en ouvrant la table attributaire de la table *t_photo*. Sélectionner par exemple la photo numéro 88 (il n'y a qu'une partie des photos dans le jeu de données).
* ![](images/photo88.png)

On peut également exploiter les relations pour consulter les photos et voir les images.
* identifier par exemple le fait F.53 de la zone de fouille nord-est.
![](images/identifierF53.png)



# 5. La saisie

La question de la saisie dans une base de données est importante. Elle correspond à une logique d'enregistrement, un ordre de description de la donnée, une façon de penser...
L'ergonomie mise en place, par l'exploitation des relations entre tables et la création de formulaires, peut faciliter et optimiser la saisie des données descriptives et géométriques.

* **Sélectionner** la table *t_interpret*. Mode Edition. 
* **Créer** l'entité **Bâtiment 8** en ajoutant une entité polygonale. Ce bâtiment se trouve dans la zone nord-est de la fouille, en limite nord de l'emprise. Il faut d'abord dessiner la géométrie, un clic droit pour la valider, puis on saisit les données descriptives tout en profitant du formulaire mis en place (liste de valeurs...). Valider le formulaire par **OK**.
![](images/creer_bat8.png)

Cette façon de procéder correspond à la manière la plus classique de créer de la donnée dans un SIG : on vectorise l'entité géométrique puis on lui attribue sa donnée descriptive (on complète la table attributaire).
Mais il on peut procéder dans l'autre sens.

* **Ouvrir la table attributaire** de la table *t_interpret*. Mode Edition.
* Choisir **Ajouter une entité** dans la barre d'outils de la table. Cela permet de créer une entité sans la géométrie, en passant directement par la table attributaire. Et OUI c'est possible !!! :satisfied:
![](images/ajout_entite.png)
* **Créer** l'entité **Bâtiment 4** en saisissant les données descriptives. Enregistrer les modifications.
![](images/creer_bat4.png)

Il y a donc, dans la même table, deux entités, dont une seule possède une géométrie.
Il est possible de rajouter la géométrie *a posteriori*.

* Dans la table attributaire de la table *t_interpret*, ou via son formulaire d'attributs, **sélectionner le Bâtiment 4**.
* Dans la barre d'outils **Numérisation avancée**, choisir **Ajouter une partie**.
![](images/ajout_partie.png)
* **Vectoriser le Bâtiment 4**. Il se trouve dans la zone sud-ouest, plus ou moins au centre de l'emprise.
![](images/vectoriser_bat4.png)

Cela a pour effet de modifier le contenu du champ "geom" de l'entité 4. Il n'y avait aucune valeur, QGIS en crée une.

Les bâtiments (*t_interpret*) sont en relation avec les faits (*t_fait*).
Afin de compléter la base de données, il faudrait mettre les uns en relation avec les autres ce qui n'est pas le cas pour l'instant.
Globalement, il y deux manières de procéder. 

:one: En passant par la table fille (*t_fait*).
* **Sélectionner** la table *t_fait*.
* **Identifier** un trou de poteau du Bâtiment 8. Mode Edition.
* **Attribuer** le fait au bâtiment 8 en complétant "numinterp" (attention à l'alias).
* Il faudrait répéter l'opération pour tous les TP du bâtiment en question. On peut aussi passer par la table attributaire de *t_fait* et compléter par lot, après une sélection (géométrique ou par expression) les valeurs dans le champ "numinterp".

:two: En passant par la table parent (*t_interpret*).
* **Sélectionner** la table *t_interpret*.
* **Identifier** le Bâtiment 8.
* Onglet Fait, où l'on a la table fille des faits.
* Mode Edition sur la table fille (en cliquant dans le formulaire du bâtiment).
* Sur la barre d'outils de la table fille, choisir **Lier les entités enfants existantes**.
![](images/lier_entites_enfants.png)
* Le formulaire des faits s'ouvre. Dans la partie de gauche il suffit de **sélectionner les faits** à mettre en relation avec le bâtiment. Il est possible de jouer avec le filtre en bas à gauche du formulaire des faits.
* Encore mieux, on peut **sélectionner les faits par la géométrie**.
![](images/select_faits_bat8.png)
* Valider par **OK**.
* **Enregistrer les modifications** de la table fille t_fait*.

Pour vérifier, il suffit de regarder les valeurs dans le champ "numinterp" des trous de poteau. 
Tout aussi facilement, il est possible de **Détacher l'entité enfant sélectionner**, ou de créer des entités avec ou sans géométries en passant directement par la relation entre les tables.

Prendre un moment pour parcourir les différentes possibilités de saisie de données descriptives et géométriques.
:warning: **Ne pas ajouter de données dans les tables *t_fait*, *t_us* et *t_mob*.**





# 6. Interroger la base de données

## 6.1. Interrogations simple et rappels

L'utilisation de QGIS permet de poser des questions assez facilement à la donnée.

On interroge le plus souvent la donnée descriptive.
Quelques exemples.
* Ouvrir la **table attributaire de *t_fait***.
* En utilisant le filtre en bas à gauche, il est certaines fois possible d'obtenir des réponses relativement rapidement.
* Choisir **Sélectionner les entités en utilisant une expression** ![](images/icon_select_epsilon.png). Par "expression", on entend bien sûr "expression SQL."
* Dans la fenêtre de Sélection par expression, il faut saisir une **Condition de requête**. Le SQL est un langage qui possède sa propre syntaxe, donc on la respecte. 
* Par exemple, on cherche on connaître quels sont les faits qui correspondent à des trous de poteau (ainsi que leur nombre).
``` sql
"ident" like 'Trou de poteau'
```
*Résultat : 300*

* Une condition s'écrit **"champ" Opérateur 'valeur'**.
* Champ avec des guillemets " ", valeur avec des apostrophes ' '.
* Il existe plusieurs **opérateurs de comparaison** en fonction du type de champ interrogé et de la question posée :
  * pour un champ **texte** (String ou Qstring).
    - ```LIKE``` : égal, comme (respect de la casse).
    - ```ILIKE``` : égal, comme (sans respecter la casse).
    - ```IS NOT``` : pas égal, différent de.
    - ```IS NULL``` : recherche la valeur NULL, on écrit pas de valeur après cet opérateur.
    - ```IS NOT NULL``` : recherche toutes les autres valeurs que NULL, on écrit pas de valeur après cet opérateur.
    - ```IN(a, b)``` : recherche dans un champ toutes les valeurs a et b.
  * pour un champ **numérique** (Integer ou Real).
    - ```=``` : égal.
    - ```<>``` : différent de.
    - ```>```: Strictement supérieur à
    - ```<``` : Strictement inférieur à
    - ```>=``` : Supérieur ou égal à
    - ```<=``` : Inférieur ou égal à
* Avec Les opérateur ```LIKE``` et ```ILIKE```, il est possible d'utiliser les caractères Joker
  * ```%``` remplace n’importe quel nombre de caractères
  * ```_``` remplace un seul caractère


Les **opérateurs logiques AND et OR**  peuvent être utilisés pour combiner les conditions de requête.
* **```AND```** : il vérifie que la condition 1 et la condition 2 sont vraies.
* **```OR```** : il vérifie que la condition 1 ou la condition 2 est vraie.
* Par exemple, on chercher à connaître les trous de poteau et les fosses.
``` sql
"ident" like 'Trou de poteau' or "ident" like 'Fosse' 
```
ou
``` sql
"ident" IN('Trou de poteau', 'Fosse') 
```
*Résultat : 417*
* Par exemple, on chercher à connaître les trous de poteau qui n'ont pas été fouillés.
``` sql
"ident" LIKE 'Trou de poteau' AND "statut" LIKE 'non fouillé'
```
*Résultat : 26*



Il est également possible de faire des recherches, d'interroger la données en passant directement par les formulaires.

* Choisir **Sélectionner des entités par valeur**. On rentre les valeurs dans les champs et on exécute. Il est possible d'exploiter la relation vers une table fille. Malheureusement, l'ergonomie n'est pas toujours au rendez-vous de ce mode d'interrogation.
![](images/select_entite_valeur.png)

On peut également interroger la géométrie. Pour cela, QGIS offre de nombreux outils.
Le plus simple est **Sélection par localisation**. 
![](images/select_par_localisation.png)

Grâce à l'utilisation de prédicat géométrique (intersecte, contient, est à l'intérieur...), on interroge une couche en la comparant à une autre. Les règles de topologie mises en place pour la géométrie sont alors très importantes puisqu'elles déterminent les relations entre les différents objets des différentes couches.

* Par exemple, on cherche le nombre de faits concernés par les Bâtiments. 
![](images/fenetre_select_par_localisation.png)
*Résultat : 18*



## 6.2. Utilisation du Gestionnaire de Base de Données

A un moment, le traitement de la donnée sera trop complexe pour être simplement réalisé dans la table attributaire d'une couche/table. 
On peut alors utiliser le Gestionnaire de Base de Données ![](images/iconDB.png). Cet outil fournit toutes les fonctionnalités d'interrogation ainsi que d'administration de base de données...

* Se connecter à la base de données SARAN en sélectionnant ![](images/icon_plume.png)**Spatialite** puis le fichier SARAN.sqlite. On a accès à toutes les tables récemment créées.
* Ouvrir la **Fenêtre SQL** ![](images/fenetreSQL.png).
Dans les faits, QGIS nous facilite la vie la plupart du temps car il écrit du SQL à notre place. Que l'on parle de symbologie ou de sélection, il y a du SQL géré par le logiciel. Même quand on définit des ensembles de règles dans la symbologie d'une couche, alors que nous écrivons des conditions de requêtes, une bonne partie de ces requêtes est gérée par QGIS. Dans le cas où les questions à poser sont complexes, sur une ou plusieurs couches, avec des calculs particuliers, il faut écrire la requête SQL du début à la fin. 
Puis faire **Exécuter**.
* La requête suivante est la plus simple (*Résultat : 615*) : 
``` sql
SELECT * 
FROM t_fait
```
On cherche ainsi à obtenir tous les enregistrements (il n'y a pas de condition), avec toutes les colonnes/champs de la table d'origine. C'est la requête de base.

![](images/requete_simple.png)

> Refaire le point sur les différentes parties du Gestionnaire

```SELECT``` sélectionne le(s) champ(s).
Le caractère ```*``` est un Joker qui permet de sélectionner tous les champs.
```FROM``` informe sur quelle table on veut travailler.

* Avec **un seul champ** :
``` sql
SELECT "num_fait" 
FROM t_fait
```
* Avec **deux champs** :
``` sql
SELECT "num_fait", "ident" 
FROM t_fait
```
On peut utiliser des alias pour faciliter la lecture et l'écriture des requêtes.
* **Sur un champ** :
``` sql
SELECT "num_fait" as Fait, "ident" 
FROM t_fait
```
* **Sur une table** :
``` sql
SELECT "num_fait" as Fait, "ident" 
FROM t_fait as f
```

En ajoutant des conditions de requête.
* Les faits qui sont **des trous de poteau** (*Résultat : 300*) :
``` sql
SELECT "num_fait" as Fait, "ident"
FROM t_fait as f
WHERE "ident" LIKE 'Trou de poteau'
```
Le ```WHERE``` permet d'extraire des entités qui respectent la condition.

* Les faits qui sont **des trous de poteau non fouillé** (*Résultat : 26*) :
``` sql
SELECT "num_fait" as Fait, "ident"
FROM t_fait as f
WHERE "ident" LIKE 'Trou de poteau' AND "statut" LIKE 'non fouillé'
```
On remarque qu'il n'est pas nécessaire d'avoir le champ après le ```SELECT``` pour l'inclure dans une condition de requête.

Il est possible d'**enregistrer les requêtes**.
* **Nommer** la dans Nom : **TP non fouillés**.
* **Enregistrer** la requête.
* Elle apparaît dans la **liste déroulante** des requêtes enregistrées.

Il est possible de regrouper, de synthétiser la donnée, tout en réalisant des calculs (tableau croisé dynamique)
* Par **type de fait** (*Résultat : 13*) :
``` sql
SELECT DISTINCT "ident" as Type
FROM t_fait as f
```
Le ```DISTINCT``` (uniquement après ```SELECT```) permet d'éviter les résultats en double. Toute la ligne est considérée. Si on rajoute le champ "num_fait", on obtient 615 car il n'y a plus de doublon/redondance.

* Avec un **décompte du nombre de faits** par type (*Résultat : 13*) :
``` sql
SELECT "ident" as Type, COUNT("ident") as NB
FROM t_fait as f
GROUP BY "ident"
```
Le ```GROUP BY``` permet de grouper les résultats selon les valeurs d'un champ. La fonction ```COUNT``` permet de compter le nombre d’enregistrements.

* **Sans les valeurs nulles** dans le champ "ident" :
``` sql
SELECT  "ident" as Type, COUNT("ident") as NB
FROM t_fait as f
WHERE "ident" IS NOT NULL
GROUP BY "ident"
```
* Avec un **tri** sur le nombre de chaque type de fait, avec les plus grosses valeurs au début :
``` sql
SELECT  "ident" as Type, COUNT("ident") as NB
FROM t_fait as f
WHERE "ident" IS NOT NULL
GROUP BY "ident"
ORDER BY NB DESC
```
```ORDER BY``` permet de trier les lignes dans le résultat. Par défaut, la commande ```ORDER BY``` utilise le suffixe ```ASC``` (sans avoir besoin de l'écrire) pour trier dans le sens ascendant. On inverse l'ordre en utilisant ```DESC```.

* Enregistrer la requête : **decompt_typ_fait**.

* En ne prenant en compte que les valeurs de **NB supérieures à 100** (*Résultat : 2*) :
``` sql
SELECT  "ident" as Type, COUNT("ident") as NB
FROM t_fait as f
WHERE "ident" IS NOT NULL  
GROUP BY "ident"
HAVING NB >100
ORDER BY NB DESC
```
```HAVING``` s'utilise après ```GROUP BY``` et permet de filtrer en utilisant les fonctions de calcul comme ```COUNT()```.

* **Charger la requête** enregistrée "TP non fouillés".


Il est souvent souhaitable de **commenter ses requêtes**, surtout quand elle commence à être complexes
``` sql
-- Quels sont les TP qui n'ont pas été fouillé
SELECT "num_fait" as Fait /* on renomme le champ pour plus de clareté */, "ident"
FROM t_fait as f
WHERE "ident" LIKE 'Trou de poteau' AND "statut" LIKE 'non fouillé'
```
Un commentaire après -- concerne tout ce qui se trouve jusqu'à la fin de la ligne.
Un commentaire entre ```/*``` et ```*/``` peut être utilisé n'importe, sur une ou plusieurs lignes.

**Enregistrer** la requête.




## 6.3. Interroger plusieurs tables

Tout l’intérêt d'une Base de données relationnelle est de pouvoir "relier" les tables entre elles grâce à des champs contenant des **clés primaires** ![pk](images/icon_pk.png) et des **clés étrangères** ![fk](images/icon_fk.png). Et c'est grâce à notre connaissance de la
structure de la BDD et de ses clés que nous allons pouvoir interroger plusieurs tables dans une même requête SQL.

*Les **jointures en SQL** permettent d’associer plusieurs tables dans une même requête. Cela permet d’exploiter la puissance des bases de données relationnelles pour obtenir des résultats qui combinent les données de plusieurs tables de manière efficace* ([sql.sh](https://sql.sh/cours/jointures)).

Il existe **plusieurs types de jointures** :
![](images/sql-join-infographie.png)

:warning: **Seules les jointures INNER JOIN et LEFT JOIN sont supportées par SQLite**.

Prendre comme point de départ la requête des TP non fouillés et enlever la condition 'non fouillé'.

* Commencer par faire une **jointure avec la table *t_us*** avec ```JOIN``` ou ```INNER JOIN``` (*Résultat : 275*) : 
``` sql
SELECT f."num_fait" as Fait 
FROM t_fait as f
JOIN t_us as u ON f."num_fait" = u."num_fait" /*les 2 champs en relation, clé primaire et clé étrancgère*/
WHERE f."ident" LIKE 'Trou de poteau' 
GROUP BY Fait /*regroupe par fait pour éviter les doublons car il y a plusieurs US par fait*/
```
Revient à demander quels sont les faits qui ont fait l'objet d'une fouille en US.

* Faire une **jointure avec la table *t_us*** avec ```LEFT JOIN``` (*Résultat : 300*) : 
``` sql
SELECT f."num_fait" as Fait 
FROM t_fait as f
LEFT JOIN t_us as u ON f."num_fait" = u."num_fait"
WHERE f."ident" LIKE 'Trou de poteau' 
GROUP BY Fait
```
On retrouve la liste des 300 TPs car même les entités de la table A sont conservés.0

* Ajouter un **décompte des US** par fait sans ```LEFT``` (*Résultat : 275*) :
``` sql
SELECT f."num_fait" as Fait, COUNT(u."num_us") as nb_us
FROM t_fait as f
JOIN t_us as u ON f."num_fait" = u."num_fait"
WHERE f."ident" LIKE 'Trou de poteau' 
GROUP BY Fait
```
**Enregistrer la requête TP avec US**

* ***BONUS*** Ajouter la liste des US par fait  (*Résultat : 275*) :
``` sql
SELECT f."num_fait" as Fait , COUNT(u."num_us") as nb_us, group_concat(u."num_us", ', ') as US /*la fonction GROUP_CONCAT() permet de regrouper les valeurs non nulles d’un groupe en une chaîne de caractère*/
FROM t_fait as f
JOIN t_us as u ON f."num_fait" = u."num_fait"
WHERE f."ident" LIKE 'Trou de poteau' 
GROUP BY Fait
```
**Enregistrer la requête TP avec US**

On peut également interroger le mobilier.
Selon la relation FAIT ↔ US ↔ Mobilier

* Ajouter la **Jointure avec la table t_mob** (*Résultat : 100*) :
``` sql
SELECT f."num_fait" as Fait , COUNT(u."num_us") as nb_us
FROM t_fait as f
JOIN t_us as u ON f."num_fait" = u."num_fait"
JOIN t_mob as m ON u."num_us" = m."us"
WHERE f."ident" LIKE 'Trou de poteau' 
GROUP BY Fait
```
Cela revient à demander parmi les 275 TPs fouillés, combien ont livré du mobilier.

* Les **TPs** ayant livré du **mobilier céramique** (*Résultat : 90*) : 
``` sql
SELECT f."num_fait" as Fait , COUNT(u."num_us") as nb_us
FROM t_fait as f
JOIN t_us as u ON f."num_fait" = u."num_fait"
JOIN t_mob as m ON u."num_us" = m."us"
WHERE f."ident" LIKE 'Trou de poteau' and m."ident" LIKE 'Céramique'
GROUP BY Fait
```
*Remarque* : pour savoir s'il y a plusieurs façons d'écrire 'céramique' dans les valeurs du champ "ident" de la table *t_mob*.

``` sql
select distinct "ident" from t_mob order by "ident"
```

* Ajouter un **décompte du nombre de restes céramiques** (*Résultat : 90*)
``` sql
SELECT f."num_fait" as Fait , COUNT(u."num_us") as nb_us, SUM(m."NR") as nr_tot
FROM t_fait as f
JOIN t_us as u ON f."num_fait" = u."num_fait"
JOIN t_mob as m ON u."num_us" = m."us"
WHERE f."ident" LIKE 'Trou de poteau' and m."ident" LIKE 'Céramique'
GROUP BY Fait
```
** Enregistrer la requête : NR_ceram_TP



## 6.4. Création d'une vue SQL

A partir de la requête précédente, il suffit d'enlever une condition et on obtient la liste des **faits** (tout type confondu) **qui ont livré du mobilier céramique** (*Résultat : 193).
``` sql
SELECT f."num_fait" as Fait , COUNT(u."num_us") as nb_us, sum(m."NR") as nr_tot
FROM t_fait as f
JOIN t_us as u ON f."num_fait" = u."num_fait"
JOIN t_mob as m ON u."num_us" = m."us"
WHERE m."ident" LIKE 'Céramique'  
GROUP BY Fait
ORDER BY nr_tot DESC /*pour les plus hautes valeurs en premier*/
```

Le tableau obtenu dans le Gestionnaire de Base de Données peut être tout simplement **copier** en cliquant sur le coin supérieur gauche comme dans n'importe lequel tableur et être **coller** dans OpenOffice ou Excel.

Un des intérêts dans le fait d'utiliser une base de données SQLite (ou même PostgreSQL bien sûr) est l'**utilisation des vues (SQL view)**. Une vue SQL dans une base de données est le résultat d'une requête SQL. Le résultat se comporte comme n'importe quelle table, avec des champs et des entités, mais ce résultat est directement dépendant des tables appelées dans la requête SQL.
Pour faire simple, si les données sont modifiées dans les tables interrogées par la requête (ajout, modification, suppression, attributaire et/ou géométrique), le résultat de la vue SQL change de façon dynamique sans avoir à exécuter à nouveau la requête.

Pour la base de données Spatialite SARAN, l'objectif est d'obtenir une vue SQL qui recense les entités qui ont livré du mobilier céramique. La requête précédente est déjà écrite, mais on souhaite observer graphiquement le résultat sur le canevas de la carte dans QGIS. Il manque donc la géométrie dans la requête.

* Ajouter la **géométrie des faits** dans la requête : 
``` sql
SELECT f."num_fait" as Fait , COUNT(u."num_us") as nb_us, sum(m."NR") as nr_tot, f."geom" as geom
FROM t_fait as f
JOIN t_us as u ON f."num_fait" = u."num_fait"
JOIN t_mob as m ON u."num_us" = m."us"
WHERE m."ident" LIKE 'Céramique'  
GROUP BY Fait
ORDER BY nr_tot DESC
```
> :warning: Si on utilise les *Virtual Layer*, il faudra appeler le champ "geometry" ; dans Spatialite, c'est le champ "geom".​ :warning:

La fonction *Charger en tant que nouvelle couche* est peu intéressante car cela va une "sorte" de Virtual Layer, gérée par le projet. 

* Ici, il faut choisir ***Créer une vue***. 
* Nommer la **v_fait_ceram**. Encore une fois, il est conseillé d'utiliser un préfixe pour distinguer cette nouvelle table des autres. Par "v_" pour vue.

![](images/creer_vue.png)

>Remarque : encore une fois, QGIS est sympa !!! Il bosse à notre place car il a, de lui-même, compléter la requête avec la commande de création de vue. Si on avait dû tout écrire, cela aurait commencé par  : ```CREATE VIEW "v_fait_ceram" AS``` 

* Il est nécessaire d'**actualiser**![](images/actualiser.png) la base SARAN.sqlite pour voir la vue SQL dans la liste des tables présentes.
* Clic droit sur la couche *v_fait_ceram* et choisir **Ajouter au canevas**.

Dès lors cette couche se comporte dans le projet QGIS comme n'importe quelle couche, à la différence qu'elle n'est pas éditable puisqu'elle est le résultat dynamique d'une requête SQL. 

On peut travailler, par exemple, sur les nombres de restes céramiques. Pour rappel, il s'agit d'une variable quantitative absolu, on peut donc utiliser la variable visuelles "Taille". C'est à dire que l'on peut faire varier la taille des formes en fonction des valeurs dans le champ "nr_tot". Or, pour des questions de lisibilités, on applique cette variable visuelle sur des données ponctuelles. On va **représenter l'effectif de céramique (NR) par en fait en cercles proportionnels**.

* Dans QGIS, déclarer un **nouveau Style**  <img src="images/boom.png" style="zoom:50%;" />

* Nommer le **cercle_prop**.

* Propriétés de la couche, onglet **Symbologie**, choisir ***Pas de symbole***.

* Onglet **Diagrammes**. Choisir ***Diagramme en camenbert***.

* Sous-onglet **Attributs**, depuis les attributs disponibles, ajouter le champ **"nr_tot"** dans les **attributs utilisés**. Changer la couleur et la légende si besoin.
  ![](images/diagram_attribut.png)

* Sous-onglet **Taille**. Choisir ***Taille variable***. Dans *Attribut*, choisir le champ "nr_tot". Puis ***Trouver*** la valeur maximale. Changer la taille en **10**.
  ![](images/diagram_taille.png)

* Sous-onglet **Légende**. Choisir ***Entrées de la légende pour la taille du diagramme***.

* Dans la fenêtre Légende pour la taille définie par des données, choisir ***Légende repliée***. Valider par **OK**.
  ![](images/diagram_legende.png)

* **Appliquer** les propriétés de la couche *v_fait_ceram*.




>Faire la démonstration : SI on modifie des données quantitatives dans la table *t_mob*, ou si on en ajoute, la taille des cercles, basés sur la géométrie des faits, est modifiée de façon dynamique.


## 6.5. Les styles

La base de données Spatialite peut enregistrer / stocker les nombreux styles déclarées pour les couches, de façon indépendante du projet. 

* Propriétés de la couche *v_fait_ceram*.
* Bouton **Style**. Choisir ***Enregistrer le style courant***.
* Dans Enregistrer le style, choisir ***Dans la base de données (Spatialite)***. 
* Nommer le style ***v_fait_ceram_cercle_prop***. Saisir une description si besoin (facultatif mais souhaitable si beaucoup de styles sont enregistrés). Il est également possible de définir le style comme étant celui par défaut.
![](images/enregistrer_style.png)

* Ouvrir le Gestionnaire de Base de Données. Se connecter à la base SARAN. Actualiser si besoin.
* Une nouvelle couche *layer_style* a été créée. Elle stocke tous les styles déclarés pour les couches et enregistrés dans la base de données. 




# Conclusion

Il faut encore une fois rappeler l'importance de la **structuration des données**. 
Les problèmes de conception de base de données et le traitement peuvent être liés en grande partie à une mauvaise structuration au départ. Il faut donc bien y réfléchir.

Une base de données Spatialite (au format .sqlite) et un projet QGIS (au format .qgz) constituent l'outil global. Le fichier Spatialite gère les données et le projet sert d'interface utilisateur. En fonction des besoins, plusieurs projets (interfaces) peuvent coexister et "appeler" la même base. La donnée reste la même et les traitements récurrents définis dans les vues SQL sont exploitables dans tous les projets. D'ailleurs, pas besoin de données pour créer des vues, une base de données peut contenir des tables vierges et des vues qui ne génèrent aucun résultat. On a juste un outil fonctionnel prêt à être utilisé pour démarrer une opération et débuter une étude.

Bien entendu, la base de données relationnelles et spatiales Spatialite n'est pas la solution miracle. Elle doit répondre certes à des problématiques structurelles et scientifiques, mais aussi à des considérations fonctionnelles et organisationnelles. Le caractère mono-utilisateur de SQLite / Spatialite peut avoir ses limites. Pour un travail collaboratif (si cela constitue l'enjeu majeur), il faudra s'orienter davantage vers la base de données PostgreSQL / PostGIS qui fonctionne en mode client-serveur.

Le langage SQL est l'un des plus anciens langages de programmation informatiques pour bases de données relationnelles. Il possède donc sa propre syntaxe et n'est au final pas très compliqué à comprendre. QGIS facilite la vie de l'utilisateur la plupart du temps en rédigeant à sa place certaines commandes ou requêtes SQL. Mais quand les questions deviennent plus complexes, il faut se lancer et écrire soi-même les requêtes. Bien souvent, on interroge la donnée attributaire en utilisant des fonctions de jointure ou d'agrégation, mais on peut également employer des fonctions géométriques afin d'interroger et traiter directement le spatial. C'est ce que l'on voit dans la formation [SIG 3.2 Analyses Spatiales](https://formationsig.gitlab.io/sig32/).

:metal::metal::metal:**MAY THE SQL BE WITH YOU** :metal::metal::metal:



# Ressources

- Le site de référence pour Spatialite \[Eng\]: <http://gaia-gis.it/gaia-sins/>
- Le diaporama des cours de @Boris Mericskay du M2 SIGAT de l'Université de Rennes 2 intitulé [Introduction au SGBD spatial avec QSpatiaLite (SQL spatial)](https://www.sites.univ-rennes2.fr/mastersigat/Cours/SeanceQSpatiaLite2.pdf)
- [Créer une base de données SpatiaLite avec QGis 2.8](<https://www.sigterritoires.fr/index.php/creer-une-base-de-donnees-spatialite-avec-qgis-2-8/>)
- Des cours et tutoriels sur le langage SQL sur le site [SQL.sh](https://sql.sh/)

